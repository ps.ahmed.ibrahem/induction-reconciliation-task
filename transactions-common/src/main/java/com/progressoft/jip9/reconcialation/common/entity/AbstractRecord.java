package com.progressoft.jip9.reconcialation.common.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

// TODO could be immutable
public abstract class AbstractRecord {
    private LocalDate date;
    private String reference;
    private BigDecimal amount;
    private String code;

    public AbstractRecord(LocalDate date, String reference, BigDecimal amount, String code) {
        this.date = date;
        this.reference = reference;
        this.amount = amount;
        this.code = code;
    }

    public AbstractRecord() {
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        AbstractRecord record = (AbstractRecord) o;
        return Objects.equals(date, record.date) &&
                Objects.equals(reference, record.reference) &&
               ( amount.compareTo(record.amount )==0) &&
                Objects.equals(code, record.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, reference, amount, code);
    }
}
