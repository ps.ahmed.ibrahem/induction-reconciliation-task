package com.progressoft.jip9.reconcialation.common.entity;

import com.progressoft.jip9.reconcialation.common.entity.*;

import java.util.ArrayList;
import java.util.List;

public class ComparisonResult {
    private List<AbstractRecord> matched;
    private List<MismatchedRecord> mismatched;
    private List<MissingRecord> missing;

    public ComparisonResult() {
        matched = new ArrayList<>();
        mismatched = new ArrayList<>();
        missing = new ArrayList<>();
    }

    public List<AbstractRecord> getMatched() {
        if (matched == null) {
            throw new NullPointerException("null matched list");
        }
        // TODO return a copy from it
        return matched;
    }

    public List<MismatchedRecord> getMismatched() {
        if (mismatched == null) {
            throw new NullPointerException("null mismatched list");
        }
        return mismatched;
    }

    public List<MissingRecord> getMissing() {
        if (missing == null) {
            throw new NullPointerException("null missing list");
        }
        return missing;
    }

    public void addMatched(AbstractRecord record) {
        validateRecordNotNull(record);
        matched.add(record);
    }

    public void addMismatched(MismatchedRecord record) {
        validateRecordNotNull(record);
        mismatched.add(record);
    }

    public void addMissing(MissingRecord record) {
        validateRecordNotNull(record);
        missing.add(record);
    }

    private void validateRecordNotNull(AbstractRecord record) {
        if (record == null) {
            throw new NullPointerException("null record");
        }
    }
}
