package com.progressoft.jip9.reconcialation.common.entity;

public enum FileType {
    CSV , JSON
}
