package com.progressoft.jip9.reconcialation.common.entity;


import java.math.BigDecimal;
import java.time.LocalDate;

public class MismatchedRecord extends AbstractRecord {
    private DestinationFile destination ;

    public MismatchedRecord(LocalDate date, String reference, BigDecimal amount, String code, DestinationFile destination) {
        super(date, reference, amount, code);
        this.destination = destination;
    }

    public MismatchedRecord(TransactionRecord record , DestinationFile destination) {
        this.destination = destination;
        super.setReference(record.getReference());
        super.setAmount(record.getAmount());
        super.setCode(record.getCode());
        super.setDate(record.getDate());
    }

    public DestinationFile getDestination() {
        return destination;
    }

    public void setDestination(DestinationFile destination) {
        this.destination = destination;
    }
}
