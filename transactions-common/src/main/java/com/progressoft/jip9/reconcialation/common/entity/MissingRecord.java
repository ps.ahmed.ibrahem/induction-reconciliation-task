package com.progressoft.jip9.reconcialation.common.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

public class MissingRecord extends AbstractRecord {
    private DestinationFile destination;

    public MissingRecord(LocalDate date, String reference, BigDecimal amount, String code, DestinationFile destination) {
        super(date, reference, amount, code);
        this.destination = destination;
    }

    public MissingRecord(TransactionRecord record , DestinationFile destination) {
        this.destination = destination;
        super.setReference(record.getReference());
        super.setAmount(record.getAmount());
        super.setCode(record.getCode());
        super.setDate(record.getDate());
    }

    public DestinationFile getDestination() {
        return destination;
    }

    public void MissingRecord(DestinationFile destination) {
        this.destination = destination;
    }
}
