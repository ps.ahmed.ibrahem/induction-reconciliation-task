package com.progressoft.jip9.reconcialation.common.entity;

import java.nio.file.Path;

public class ReconciliationRequest {
    Path sourceUrl ;
    FileType sourceType ;
    Path targetUrl ;
    FileType targetType ;

    public ReconciliationRequest(Path sourceUrl, FileType sourceType, Path targetUrl, FileType targetType) {
        this.sourceUrl = sourceUrl;
        this.sourceType = sourceType;
        this.targetUrl = targetUrl;
        this.targetType = targetType;
    }

    public void setSourceUrl(Path sourceUrl){
        this.sourceUrl = sourceUrl ;
    }

    public Path getSourceUrl() {
        return sourceUrl;
    }

    public FileType getSourceType() {
        return sourceType;
    }

    public void setSourceType(FileType sourceType) {
        this.sourceType = sourceType;
    }

    public Path getTargetUrl() {
        return targetUrl;
    }

    public void setTargetUrl(Path targetUrl) {
        this.targetUrl = targetUrl;
    }

    public FileType getTargetType() {
        return targetType;
    }

    public void setTargetType(FileType targetType) {
        this.targetType = targetType;
    }
}
