package com.progressoft.jip9.reconcialation.common.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

public class TransactionRecord extends AbstractRecord{
    public TransactionRecord(LocalDate date, String reference, BigDecimal amount, String code) {
        super(date, reference, amount, code);
    }

    public TransactionRecord() {
    }
}
