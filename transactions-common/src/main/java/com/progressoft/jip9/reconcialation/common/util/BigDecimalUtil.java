package com.progressoft.jip9.reconcialation.common.util;

import java.math.BigDecimal;

public class BigDecimalUtil {

    public static BigDecimal getBigDecimal(String value){
        if (value == null || value.isEmpty()){
            throw new IllegalArgumentException("missing amount");
        }
        if (Double.parseDouble(value) <=0){
            throw new IllegalArgumentException("invalid amount : " +value);
        }
        try{
            return new BigDecimal(value);
        }
        catch (Exception e){
            throw new IllegalArgumentException("invalid amount : " + value);
        }
    }
}
