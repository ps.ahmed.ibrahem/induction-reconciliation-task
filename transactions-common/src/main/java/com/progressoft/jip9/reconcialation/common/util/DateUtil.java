package com.progressoft.jip9.reconcialation.common.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class DateUtil {

    public static LocalDate getDate(String date){
        if (date == null || date.isEmpty()) {
            throw new IllegalArgumentException("missing date");
        }
        try {
            return LocalDate.parse(date );
        }
        catch (Exception e ){
            try {
                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy" );
                return LocalDate.parse(date ,formatter );
            }
            catch (Exception e2){
                throw new IllegalArgumentException("invalid date");
            }
        }
    }
}
