package com.progressoft.jip9.reconcialation.common.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.Map;

public class JSONUtil {

    public static List<Map<String,String>> getValues(String content){
        ObjectMapper mapper = new ObjectMapper();

        try {
            return mapper.readValue(content, new TypeReference<List<Map<String, String>>>(){});
        } catch (JsonProcessingException e) {
            throw new IllegalArgumentException("invalid JSON format");
        }
    }
}
