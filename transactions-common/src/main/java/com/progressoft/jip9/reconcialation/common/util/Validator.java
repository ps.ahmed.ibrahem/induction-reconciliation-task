package com.progressoft.jip9.reconcialation.common.util;

import com.progressoft.jip9.reconcialation.common.entity.AbstractRecord;


import java.util.Currency;

public class Validator {
    public static void validateContentNotNull(String content) {
        if (content == null) {
            throw new IllegalArgumentException("null data");
        }
        if (content.isEmpty()) {
            throw new IllegalArgumentException("empty data");
        }
    }


    public static void validateRecord(AbstractRecord record) {
        validateReference(record.getReference());
        validateCurrencyCode(record.getCode());
    }

    private static void validateReference(String reference) {
        if (reference == null) {
            throw new IllegalArgumentException("null reference");
        }
        if (reference.isEmpty()) {
            throw new IllegalArgumentException("missing reference");
        }
    }

    private static void validateCurrencyCode(String code) {
        if (code == null || code.isEmpty()){
            throw new IllegalArgumentException("missing currency code");
        }
        try {
            Currency.getInstance(code);
        } catch (Exception e) {
            throw new IllegalArgumentException("invalid currency");
        }
    }


}
