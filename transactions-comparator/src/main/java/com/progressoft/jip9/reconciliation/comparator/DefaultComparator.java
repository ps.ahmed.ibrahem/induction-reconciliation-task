package com.progressoft.jip9.reconciliation.comparator;

import com.progressoft.jip9.reconcialation.common.entity.*;
import com.progressoft.jip9.reconciliation.comparator.util.ListsContainer;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Currency;
import java.util.List;

public class DefaultComparator implements TransactionComparator {

    // TODO should be a local variable
    private ComparisonResult result;

    @Override
    public ComparisonResult compare(List<TransactionRecord> source, List<TransactionRecord> target) {
        validateInputs(source, target);
        result = new ComparisonResult();
        ListsContainer container = new ListsContainer(source, target);

        for (TransactionRecord sourceRecord : container.getSourceList()) {
            for (TransactionRecord targetRecord : container.getTargetList()) {
                if (isMatched(sourceRecord, targetRecord)) {
                    storeMatched(container, sourceRecord, targetRecord);
                } else if (isMismatched(sourceRecord, targetRecord)) {
                    storeMismatched(container, sourceRecord, targetRecord);
                }
            }
        }
        storeMissingSourceRecords(container.getTempSourceList());
        storeMissingTargetRecords(container.getTempTargetList());

        return result;
    }

    private BigDecimal getScaledAmount(TransactionRecord sourceRecord) {
        return sourceRecord.getAmount().setScale(getDefaultFractionDigits(sourceRecord), RoundingMode.HALF_UP);
    }

    private int getDefaultFractionDigits(TransactionRecord sourceRecord) {
        return Currency.getInstance(sourceRecord.getCode()).getDefaultFractionDigits();
    }

    private void storeMismatched(ListsContainer container, TransactionRecord sourceRecord, TransactionRecord targetRecord) {
        sourceRecord.setAmount(getScaledAmount(sourceRecord));
        targetRecord.setAmount(getScaledAmount(targetRecord));

        MismatchedRecord sourceMismatched = new MismatchedRecord(sourceRecord, DestinationFile.SOURCE);
        MismatchedRecord targetMismatched = new MismatchedRecord(targetRecord, DestinationFile.TARGET);
        result.addMismatched(sourceMismatched);
        result.addMismatched(targetMismatched);
        container.getTempSourceList().remove(sourceRecord);
        container.getTempTargetList().remove(targetRecord);
    }

    private void storeMatched(ListsContainer container, TransactionRecord sourceRecord, TransactionRecord targetRecord) {
        sourceRecord.setAmount(getScaledAmount(sourceRecord));
        result.addMatched(sourceRecord);
        container.getTempSourceList().remove(sourceRecord);
        container.getTempTargetList().remove(targetRecord);

    }

    private boolean isMismatched(TransactionRecord sourceRecord, TransactionRecord targetRecord) {
        return isMatchedReference(sourceRecord, targetRecord)
                && !isMatched(sourceRecord, targetRecord);
    }

    private boolean isMatched(TransactionRecord sourceRecord, TransactionRecord targetRecord) {
        return sourceRecord.equals(targetRecord);
    }

    private boolean isMatchedReference(TransactionRecord source, TransactionRecord target) {
        return source.getReference().equalsIgnoreCase(target.getReference());
    }

    private void storeMissingTargetRecords(List<TransactionRecord> target) {
        for (TransactionRecord missingTarget : target) {
            missingTarget.setAmount(getScaledAmount(missingTarget));
            result.addMissing(new MissingRecord(missingTarget, DestinationFile.TARGET));
        }
    }

    private void storeMissingSourceRecords(List<TransactionRecord> source) {
        for (TransactionRecord missingSource : source) {
            missingSource.setAmount(getScaledAmount(missingSource));
            result.addMissing(new MissingRecord(missingSource, DestinationFile.SOURCE));
        }
    }

    private void validateInputs(List<TransactionRecord> source, List<TransactionRecord> target) {
        if (source == null) {
            throw new IllegalArgumentException("null source");
        }
        if ((source.isEmpty())) {
            throw new IllegalArgumentException("empty source");

        }

        if (target == null) {
            throw new IllegalArgumentException("null target");
        }
        if ((target.isEmpty())) {
            throw new IllegalArgumentException("empty target");

        }
    }
}
