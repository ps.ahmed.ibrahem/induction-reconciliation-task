package com.progressoft.jip9.reconciliation.comparator;

import com.progressoft.jip9.reconcialation.common.entity.ComparisonResult;
import com.progressoft.jip9.reconcialation.common.entity.TransactionRecord;

import java.util.List;

public interface TransactionComparator {
    ComparisonResult compare(List<TransactionRecord> source, List<TransactionRecord> target);
}
