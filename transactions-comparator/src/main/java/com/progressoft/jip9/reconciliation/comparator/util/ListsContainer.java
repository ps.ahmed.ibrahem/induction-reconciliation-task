package com.progressoft.jip9.reconciliation.comparator.util;

import com.progressoft.jip9.reconcialation.common.entity.TransactionRecord;

import java.util.ArrayList;
import java.util.List;

public class ListsContainer {
    private final List<TransactionRecord> sourceList;
    private final List<TransactionRecord> targetList;
    private List<TransactionRecord> tempSourceList;
    private List<TransactionRecord> tempTargetList;

    public ListsContainer(List<TransactionRecord> sourceRecords, List<TransactionRecord> targetRecords) {
        this.sourceList = sourceRecords;
        this.targetList = targetRecords;
        this.tempSourceList = new ArrayList<>(sourceRecords);
        this.tempTargetList = new ArrayList<>(targetRecords);
    }

    public List<TransactionRecord> getSourceList() {
        return sourceList;
    }

    public List<TransactionRecord> getTargetList() {
        return targetList;
    }

    public List<TransactionRecord> getTempSourceList() {
        return tempSourceList;
    }

    public List<TransactionRecord> getTempTargetList() {
        return tempTargetList;
    }
}
