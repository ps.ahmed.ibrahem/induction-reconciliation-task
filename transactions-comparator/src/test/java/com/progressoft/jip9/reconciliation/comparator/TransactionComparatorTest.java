package com.progressoft.jip9.reconciliation.comparator;

import com.progressoft.jip9.reconcialation.common.entity.*;
import com.progressoft.jip9.reconcialiation.parser.CsvParser;
import com.progressoft.jip9.reconcialiation.parser.JsonParser;
import com.progressoft.jip9.reconcialiation.parser.Parser;
import com.progressoft.jip9.reconcialiation.reader.CsvFileReader;
import com.progressoft.jip9.reconcialiation.reader.JsonFileReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TransactionComparatorTest {
    private final TransactionRecord sampleRecord = new TransactionRecord
            (LocalDate.parse("2019-05-26"), "TR-0001", new BigDecimal("20"), "JOD");
    private final TransactionRecord sampleRecord2 = new TransactionRecord
            (LocalDate.parse("2019-05-26"), "TR-0001", new BigDecimal("20.000"), "JOD");

    private static final Path inputPath = Paths.get("/home/user/work/induction/jip-9-javase/induction-reconciliation-task/sample-files/input-files/");
    private static final Path jsonFile = inputPath.resolve("online-banking-transactions.json");
    private static final Path csvFile = inputPath.resolve("bank-transactions.csv");

    private static List<TransactionRecord> csvRecords;
    private static List<TransactionRecord> jsonRecords;


    @BeforeAll
    public static void initiateData() throws NoSuchFileException {
        Parser csvParser = new CsvParser();
        Parser jsonParser = new JsonParser();

        csvRecords = csvParser.parseToList(new CsvFileReader().getContent(csvFile));
        jsonRecords = jsonParser.parseToList(new JsonFileReader().getContent(jsonFile));

    }

    @Test
    public void givenNullSource_whenCompare_thenFail() {
        TransactionComparator comparator = new DefaultComparator();
        List<TransactionRecord> source = null;
        List<TransactionRecord> target = Arrays.asList(sampleRecord);

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> comparator.compare(source, target));

        Assertions.assertEquals("null source", exception.getMessage());
    }

    @Test
    public void givenEmptySource_whenCompare_thenFail() {
        TransactionComparator comparator = new DefaultComparator();
        List<TransactionRecord> source = new ArrayList<>(1);
        List<TransactionRecord> target = Arrays.asList(sampleRecord);

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> comparator.compare(source, target));

        Assertions.assertEquals("empty source", exception.getMessage());
    }

    @Test
    public void givenNullTarget_whenCompare_thenFail() {
        TransactionComparator comparator = new DefaultComparator();
        List<TransactionRecord> source = Arrays.asList(sampleRecord);
        List<TransactionRecord> target = null;

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> comparator.compare(source, target));

        Assertions.assertEquals("null target", exception.getMessage());
    }

    @Test
    public void givenEmptyTarget_whenCompare_thenFail() {
        TransactionComparator comparator = new DefaultComparator();
        List<TransactionRecord> source = Arrays.asList(sampleRecord);
        List<TransactionRecord> target = new ArrayList<>(1);

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> comparator.compare(source, target));

        Assertions.assertEquals("empty target", exception.getMessage());
    }

    @Test
    public void givenValidInputs_whenCompare_thenMatchingResultShouldBeAsExpected() {
        ComparisonResult results = new DefaultComparator().compare(csvRecords, jsonRecords);
        List<AbstractRecord> matched = results.getMatched();

        int expectedListSize = 3;
        int actualListSize = matched.size();
        Assertions.assertEquals(expectedListSize, actualListSize, "list size not as expected");

        String reference = "TR-47884222201";
        BigDecimal amount = BigDecimal.valueOf(140);
        String code = "USD";
        LocalDate date = LocalDate.parse("2020-01-20");
        Assertions.assertEquals(reference, matched.get(0).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(matched.get(0).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, matched.get(0).getCode(), "code not as expected");
        Assertions.assertEquals(date, matched.get(0).getDate(), "date not as expected");

        reference = "TR-47884222203";
        amount = BigDecimal.valueOf(5000.000);
        code = "JOD";
        date = LocalDate.parse("2020-01-25");
        Assertions.assertEquals(reference, matched.get(1).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(matched.get(1).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, matched.get(1).getCode(), "code not as expected");
        Assertions.assertEquals(date, matched.get(1).getDate(), "date not as expected");

        reference = "TR-47884222206";
        amount = BigDecimal.valueOf(500.00);
        code = "USD";
        date = LocalDate.parse("2020-02-10");
        Assertions.assertEquals(reference, matched.get(2).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(matched.get(2).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, matched.get(2).getCode(), "code not as expected");
        Assertions.assertEquals(date, matched.get(2).getDate(), "date not as expected");
    }

    @Test
    public void givenValidInputs_whenCompare_thenMismatchingResultShouldBeAsExpected() {
        ComparisonResult results = new DefaultComparator().compare(csvRecords, jsonRecords);
        List<MismatchedRecord> mismatched = results.getMismatched();

        int expectedListSize = 4;
        int actualListSize = mismatched.size();
        Assertions.assertEquals(expectedListSize, actualListSize, "list size not as expected");

        DestinationFile  destination = DestinationFile.SOURCE ;
        String reference = "TR-47884222202";
        BigDecimal amount = BigDecimal.valueOf(20.000);
        String code = "JOD";
        LocalDate date = LocalDate.parse("2020-01-22");
        Assertions.assertEquals(reference, mismatched.get(0).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(mismatched.get(0).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, mismatched.get(0).getCode(), "code not as expected");
        Assertions.assertEquals(date, mismatched.get(0).getDate(), "date not as expected");
        Assertions.assertEquals(destination, mismatched.get(0).getDestination(), "destenation not as expected");

        destination = DestinationFile.TARGET ;
        reference = "TR-47884222202";
        amount = BigDecimal.valueOf(30.000);
        code = "JOD";
        date = LocalDate.parse("2020-01-22");
        Assertions.assertEquals(reference, mismatched.get(1).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(mismatched.get(1).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, mismatched.get(1).getCode(), "code not as expected");
        Assertions.assertEquals(date, mismatched.get(1).getDate(), "date not as expected");
        Assertions.assertEquals(destination, mismatched.get(1).getDestination(), "destenation not as expected");

        destination = DestinationFile.SOURCE ;
        reference = "TR-47884222205";
        amount = BigDecimal.valueOf(60.000);
        code = "JOD";
        date = LocalDate.parse("2020-02-02");
        Assertions.assertEquals(reference, mismatched.get(2).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(mismatched.get(2).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, mismatched.get(2).getCode(), "code not as expected");
        Assertions.assertEquals(date, mismatched.get(2).getDate(), "date not as expected");
        Assertions.assertEquals(destination, mismatched.get(2).getDestination(), "destenation not as expected");

        destination = DestinationFile.TARGET ;
        reference = "TR-47884222205";
        amount = BigDecimal.valueOf(60.000);
        code = "JOD";
        date = LocalDate.parse("2020-02-03");
        Assertions.assertEquals(reference, mismatched.get(3).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(mismatched.get(3).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, mismatched.get(3).getCode(), "code not as expected");
        Assertions.assertEquals(date, mismatched.get(3).getDate(), "date not as expected");
        Assertions.assertEquals(destination, mismatched.get(3).getDestination(), "destination not as expected");

    }

    @Test
    public void givenValidInputs_whenCompare_thenMissingResultShouldBeAsExpected() {
        ComparisonResult results = new DefaultComparator().compare(csvRecords, jsonRecords);
        List<MissingRecord> mismatched = results.getMissing();

        int expectedListSize = 3;
        int actualListSize = mismatched.size();
        Assertions.assertEquals(expectedListSize, actualListSize, "list size not as expected");

        DestinationFile  destination = DestinationFile.SOURCE ;
        String reference = "TR-47884222204";
        BigDecimal amount = BigDecimal.valueOf(1200.000);
        String code = "JOD";
        LocalDate date = LocalDate.parse("2020-01-31");
        Assertions.assertEquals(reference, mismatched.get(0).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(mismatched.get(0).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, mismatched.get(0).getCode(), "code not as expected");
        Assertions.assertEquals(date, mismatched.get(0).getDate(), "date not as expected");
        Assertions.assertEquals(destination, mismatched.get(0).getDestination(), "destenation not as expected");

        destination = DestinationFile.TARGET ;
        reference = "TR-47884222217";
        amount = BigDecimal.valueOf(12000.000);
        code = "JOD";
        date = LocalDate.parse("2020-02-14");
        Assertions.assertEquals(reference, mismatched.get(1).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(mismatched.get(1).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, mismatched.get(1).getCode(), "code not as expected");
        Assertions.assertEquals(date, mismatched.get(1).getDate(), "date not as expected");
        Assertions.assertEquals(destination, mismatched.get(1).getDestination(), "destenation not as expected");

        destination = DestinationFile.TARGET ;
        reference = "TR-47884222245";
        amount = BigDecimal.valueOf(420.00);
        code = "USD";
        date = LocalDate.parse("2020-01-12");
        Assertions.assertEquals(reference, mismatched.get(2).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(mismatched.get(2).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, mismatched.get(2).getCode(), "code not as expected");
        Assertions.assertEquals(date, mismatched.get(2).getDate(), "date not as expected");
        Assertions.assertEquals(destination, mismatched.get(2).getDestination(), "destenation not as expected");

    }

    @Test
    public void givenTwoIdenticalRecords_whenEqual_thenShouldReturnTrue() {
        Assertions.assertTrue(sampleRecord.equals(sampleRecord2));
    }

}
