package com.progressoft.jip9.reconciliation.exporter;

import com.progressoft.jip9.reconcialation.common.entity.AbstractRecord;
import com.progressoft.jip9.reconcialation.common.entity.ComparisonResult;
import com.progressoft.jip9.reconcialation.common.entity.MismatchedRecord;
import com.progressoft.jip9.reconcialation.common.entity.MissingRecord;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;

public class CsvExporter implements Exporter {
    @Override
    public void export(Path directory, ComparisonResult result) {
        validateNotNull(result);
        validateDirectory(directory);
        Path outputDir = createIfNotExist(directory);
        exportMatchedFile(result,  createFileIfNotExist(outputDir, "matching-transactions.csv"));
        exportMismatchedFile(result, createFileIfNotExist(outputDir, "mismatched-transactions.csv"));
        exportMissingFile(result , createFileIfNotExist(outputDir, "missing-transactions.csv"));

    }

    private void exportMismatchedFile(ComparisonResult result, File mismatchedFile) {
        try (PrintWriter writer = new PrintWriter(new FileOutputStream(mismatchedFile), true)) {
            String header = "found in file,transaction id,amount,currency code,date";
            writer.println(header);
            for (MismatchedRecord record : result.getMismatched()) {
                String line = record.getDestination() + "," + record.getReference() + "," + record.getAmount() + "," + record.getCode() + "," + record.getDate();
                writer.println(line);
            }
        } catch (IOException e) {
            throw new RuntimeException("could not write mismatched file");
        }
    }

    private void exportMissingFile(ComparisonResult result, File missingFile) {
        try (PrintWriter writer = new PrintWriter(new FileOutputStream(missingFile), true)) {
            String header = "found in file,transaction id,amount,currency code,date";
            writer.println(header);
            for (MissingRecord record : result.getMissing()) {
                String line = record.getDestination() + "," + record.getReference() + "," + record.getAmount() + "," + record.getCode() + "," + record.getDate();
                writer.println(line);
            }
        } catch (IOException e) {
            throw new RuntimeException("could not write missing file");
        }
    }

    private void exportMatchedFile(ComparisonResult result, File matchedFile) {
        try (PrintWriter writer = new PrintWriter(new FileOutputStream(matchedFile), true)) {
            String header = "transaction id,amount,currecny code,date";
            writer.println(header);
            for (AbstractRecord record : result.getMatched()) {
                String line = record.getReference() + "," + record.getAmount() + "," + record.getCode() + "," + record.getDate();
                writer.println(line);
            }
        } catch (IOException e) {
            throw new RuntimeException("could not write matched file");
        }
    }

    private File createFileIfNotExist(Path outputDir, String fileName) {
        File matchedFile = new File(outputDir + "/" + fileName);
        try {
            matchedFile.createNewFile();
        } catch (IOException e) {
            throw new RuntimeException("could not create file");
        }
        return matchedFile;
    }

    private Path createIfNotExist(Path directory) {
        Path path;
        if (Files.exists(directory)) {
            path = directory;
        } else {
            try {
                path = Files.createDirectories(directory);
            } catch (IOException e) {
                throw new RuntimeException("could not create directory");
            }
        }
        return path;
    }

    private void validateNotNull(ComparisonResult result) {
        if (result == null) {
            throw new IllegalArgumentException("null result");
        }
    }

    private void validateDirectory(Path directory) {
        if (directory == null) {
            throw new IllegalArgumentException("null directory");

        }
    }
}
