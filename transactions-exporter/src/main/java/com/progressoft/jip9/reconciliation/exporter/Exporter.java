package com.progressoft.jip9.reconciliation.exporter;

import com.progressoft.jip9.reconcialation.common.entity.ComparisonResult;

import java.nio.file.Path;

public interface Exporter {
    void export(Path directory, ComparisonResult result);
}
