package com.progressoft.jip9.reconciliation.exporter;

import com.progressoft.jip9.reconcialation.common.entity.ComparisonResult;
import com.progressoft.jip9.reconcialation.common.entity.TransactionRecord;
import com.progressoft.jip9.reconcialiation.parser.CsvParser;
import com.progressoft.jip9.reconcialiation.parser.JsonParser;
import com.progressoft.jip9.reconcialiation.reader.CsvFileReader;
import com.progressoft.jip9.reconcialiation.reader.JsonFileReader;
import com.progressoft.jip9.reconcialiation.reader.TransactionReader;
import com.progressoft.jip9.reconciliation.comparator.DefaultComparator;
import com.progressoft.jip9.reconciliation.comparator.TransactionComparator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;


public class CsvExporterTest {

    private static final Path inputPath = Paths.get("/home/user/work/induction/jip-9-javase/induction-reconciliation-task/sample-files/input-files/");
    private static final Path jsonFile = inputPath.resolve("online-banking-transactions.json");
    private static final Path csvFile = inputPath.resolve("bank-transactions.csv");

    private static final Path resultPath = Paths.get("/home/user/work/induction/jip-9-javase/induction-reconciliation-task/sample-files/result-files/");
    private static final Path matchingFile = resultPath.resolve("matching-transactions.csv");
    private static final Path mismatchedFile = resultPath.resolve("mismatched-transactions.csv");
    private static final Path missingFile = resultPath.resolve("missing-transactions.csv");




    @Test
    public void givenNullComparisonResultAndValidPath_whenExport_thenFail() {
        ComparisonResult result = null;
        Path directory = Paths.get("/home/user/work/");

        Exporter exporter = new CsvExporter();

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> exporter.export(directory, result));

        Assertions.assertEquals("null result" , exception.getMessage());
    }

    @Test
    public void givenNullPathAndValidResult_whenExport_thenFail(){
        ComparisonResult result = new ComparisonResult();
        Path directory = null;

        Exporter exporter = new CsvExporter();

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> exporter.export(directory, result));

        Assertions.assertEquals("null directory" , exception.getMessage());
    }

    @Test
    public void givenValidInput_whenExport_thenExportedMatchedFileShouldBeAsExpected(@TempDir Path tempDir) throws IOException {
        ComparisonResult result = prepareResult();

        Exporter exporter = new CsvExporter();
        exporter.export(tempDir , result);

        List<String> expectedContent = Files.readAllLines(matchingFile);
        List<String> actualContent = Files.readAllLines(tempDir.resolve("matching-transactions.csv"));

        Assertions.assertEquals(expectedContent.size() , actualContent.size() , "size not as expected");
        Assertions.assertEquals(expectedContent , actualContent , "result not as expected");

    }

    @Test
    public void givenValidInput_whenExport_thenExportedMismatchedFileShouldBeAsExpected(@TempDir Path tempDir) throws IOException {
        ComparisonResult result = prepareResult();

        Exporter exporter = new CsvExporter();
        exporter.export(tempDir , result);

        List<String> expectedContent = Files.readAllLines(mismatchedFile);
        List<String> actualContent = Files.readAllLines(tempDir.resolve("mismatched-transactions.csv"));

        Assertions.assertEquals(expectedContent.size() , actualContent.size() , "size not as expected");
        Assertions.assertEquals(expectedContent , actualContent , "result not as expected");

    }

    @Test
    public void givenValidInput_whenExport_thenExportedMissingFileShouldBeAsExpected(@TempDir Path tempDir) throws IOException {
        ComparisonResult result = prepareResult();

        Exporter exporter = new CsvExporter();
        exporter.export(tempDir , result);

        List<String> expectedContent = Files.readAllLines(missingFile);
        List<String> actualContent = Files.readAllLines(tempDir.resolve("missing-transactions.csv"));

        Assertions.assertEquals(expectedContent.size() , actualContent.size() , "size not as expected");
        Assertions.assertEquals(expectedContent , actualContent , "result not as expected");

    }



    private ComparisonResult prepareResult() throws NoSuchFileException {
        TransactionReader csvReader = new CsvFileReader();
        TransactionReader jsonReader = new JsonFileReader();

        List<TransactionRecord> sourceRecords = new CsvParser().parseToList(csvReader.getContent(csvFile));
        List<TransactionRecord> targetRecords = new JsonParser().parseToList(jsonReader.getContent(jsonFile));

        TransactionComparator comparator = new DefaultComparator();

        return comparator.compare(sourceRecords, targetRecords);
    }

}
