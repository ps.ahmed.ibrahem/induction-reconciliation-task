package com.progressoft.jip9.reconcialiation.parser;

import com.progressoft.jip9.reconcialation.common.entity.TransactionRecord;
import com.progressoft.jip9.reconcialation.common.util.BigDecimalUtil;
import com.progressoft.jip9.reconcialation.common.util.DateUtil;
import com.progressoft.jip9.reconcialation.common.util.Validator;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CsvParser implements Parser {

    public List<TransactionRecord> parseToList(String content) {
        Validator.validateContentNotNull(content);
        return getTransactionRecords(content);
    }

// TODO why loading all file content as string, you can use inputstream directly here
    private List<TransactionRecord> getTransactionRecords(String content) {
        List<TransactionRecord> records = new ArrayList<>();
        String[] lines = content.split("\n");

        List<String> header = Arrays.asList(lines[0].split(","));
        int referenceIndex = header.indexOf("transaction id");
        int codeIndex = header.indexOf("currency");
        int dateIndex = header.indexOf("date");
        int amountIndex = header.indexOf("amount");

        for (int lineNumber = 1; lineNumber < lines.length; lineNumber++) {
            String[] attributes = lines[lineNumber].split(",");
            validateCsvFormat(header, attributes);

            LocalDate date = DateUtil.getDate(attributes[dateIndex]);
            String reference = attributes[referenceIndex];
            BigDecimal amount = BigDecimalUtil.getBigDecimal(attributes[amountIndex]);
            String code = attributes[codeIndex];

            TransactionRecord record = new TransactionRecord(date, reference, amount, code);
            Validator.validateRecord(record);
            records.add(record);
        }
        return records;
    }


    private void validateCsvFormat(List<String> header, String[] attributes) {
        int attributeCount = header.size();
        if (attributes.length != attributeCount) {
            throw new IllegalArgumentException("invalid CSV format");
        }
    }

}
