package com.progressoft.jip9.reconcialiation.parser;

import com.progressoft.jip9.reconcialation.common.entity.TransactionRecord;
import com.progressoft.jip9.reconcialation.common.util.BigDecimalUtil;
import com.progressoft.jip9.reconcialation.common.util.DateUtil;
import com.progressoft.jip9.reconcialation.common.util.JSONUtil;
import com.progressoft.jip9.reconcialation.common.util.Validator;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class JsonParser implements  Parser{

    @Override
    public List<TransactionRecord> parseToList(String content) {
        Validator.validateContentNotNull(content);
        return getTransactionRecords(content);
    }

    private List<TransactionRecord> getTransactionRecords(String content) {
        List<TransactionRecord> records = new ArrayList<>();

        List<Map<String, String>> data = JSONUtil.getValues(content);

        for (Map<String , String> transaction : data) {
            String reference = transaction.get("reference");
            String code = transaction.get("currencyCode");
            BigDecimal amount = BigDecimalUtil.getBigDecimal(transaction.get("amount"));
            LocalDate date = DateUtil.getDate(transaction.get("date"));

            TransactionRecord record = new TransactionRecord(date,reference ,amount ,code);
            Validator.validateRecord(record);
            records.add(record);
        }
        return records;
    }

}
