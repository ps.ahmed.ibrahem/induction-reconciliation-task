package com.progressoft.jip9.reconcialiation.parser;

import com.progressoft.jip9.reconcialation.common.entity.TransactionRecord;

import java.util.List;

public interface Parser {
     List<TransactionRecord> parseToList(String content);
}
