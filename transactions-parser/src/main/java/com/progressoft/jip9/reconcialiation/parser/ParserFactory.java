package com.progressoft.jip9.reconcialiation.parser;

import com.progressoft.jip9.reconcialation.common.entity.FileType;

public class ParserFactory {

    public static Parser getParser(FileType type){
        switch (type){
            case CSV: return new CsvParser();
            case JSON:return new JsonParser();
            default: throw new IllegalArgumentException("file type not supported");
        }
    }
}
