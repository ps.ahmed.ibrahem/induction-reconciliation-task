package com.progressoft.jip9.reconcialiation.parser;

import com.progressoft.jip9.reconcialation.common.entity.TransactionRecord;
import com.progressoft.jip9.reconcialiation.reader.CsvFileReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.List;

public class CsvParserTest {
    private final Path path = Paths.get("/home/user/work/induction/jip-9-javase/induction-reconciliation-task/sample-files/input-files/");
    private final Path file = path.resolve("bank-transactions.csv");


    @Test
    public void givenNullString_whenParseToList_thenFail() {
        String content = null;
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CsvParser().parseToList(content));

        Assertions.assertEquals("null data", exception.getMessage());
    }

    @Test
    public void givenEmptyString_whenParseToList_thenFail() {
        String content = "";
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CsvParser().parseToList(content));

        Assertions.assertEquals("empty data", exception.getMessage());
    }

    @Test
    public void givenInvalidCsvString_whenParseToList_thenFail() {
        String content = "transaction id,trans description,amount,currency,purpose,date,trans type\n" +
                "TR-47884222201,online transfer,140,USD,donation,2020-01-20,D\n" +
                "TR-47884222202,atm withdrwal,20.0000,JOD,,2020-01-22,D\n" +
                "counter withdrawal,5000,JOD,,2020-01-25,D\n" +
                "TR-47884222204,salary,1200.000,JOD,donation,2020-01-31,C\n" +
                "60.0,JOD,,2020-02-02,D\n" +
                "TR-47884222206,atm withdrwal,500.0,USD,,2020-02-10,D";

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CsvParser().parseToList(content));

        Assertions.assertEquals("invalid CSV format", exception.getMessage());

    }
    @Test
    public void givenValidCsvAndEmptyReference_whenParseToList_thenFail() {
        String content ="transaction id,trans description,amount,currency,purpose,date,trans type\n" +
                ",online transfer,140,USD,donation,2020-01-20,D";

        Parser parser = new CsvParser();

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> parser.parseToList(content));

        Assertions.assertEquals("missing reference", exception.getMessage());

    }

    @Test
    public void givenValidCsvAndEmptyCode_whenParseToList_thenFail() {
        String content ="transaction id,trans description,amount,currency,purpose,date,trans type\n" +
                ",online transfer,140,,donation,2020-01-20,D";

        Parser parser = new CsvParser();

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> parser.parseToList(content));

        Assertions.assertEquals("missing reference", exception.getMessage());

    }

    @Test
    public void givenCsvStringWithEmptyReference_whenParseToList_thenFail() {
        String content = "transaction id,trans description,amount,currency,purpose,date,trans type\n" +
                ",online transfer,140,USD,donation,2020-01-20,D";
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CsvParser().parseToList(content));

        Assertions.assertEquals("missing reference", exception.getMessage());

    }

    @Test
    public void givenCsvStringWithInvalidCurrencyCode_whenParseToList_thenFail() {
        String content = "transaction id,trans description,amount,currency,purpose,date,trans type\n" +
                "TR-001,online transfer,140,XYZ,donation,2020-01-20,D";
        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CsvParser().parseToList(content));

        Assertions.assertEquals("invalid currency", exception.getMessage());

    }

    @Test
    public void givenCsvStringWithInvalidCurrency_whenParseToList_thenFail() {
        String content = "transaction id,trans description,amount,currency,purpose,date,trans type\n" +
                "TR-47884222201,online transfer,140,XYZ,donation,2020-01-20,D";


        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CsvParser().parseToList(content));

        Assertions.assertEquals("invalid currency", exception.getMessage());

    }

    @Test
    public void givenCsvStringWithEmptyDate_whenParseToList_thenFail() {
        String content = "transaction id,trans description,amount,currency,purpose,date,trans type\n" +
                "TR-47884222201,online transfer,140,USD,donation,,D";


        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CsvParser().parseToList(content));

        Assertions.assertEquals("missing date", exception.getMessage());

    }
    @Test
    public void givenCsvStringWithEmptyAmount_whenParseToList_thenFail() {
        String content = "transaction id,trans description,amount,currency,purpose,date,trans type\n" +
                "TR-47884222201,online transfer,,USD,donation,2020-02-02,D";


        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CsvParser().parseToList(content));

        Assertions.assertEquals("missing amount", exception.getMessage());

    }

    @Test
    public void givenCsvStringWithInvalidAmount_whenParseToList_thenFail() {
        String content = "transaction id,trans description,amount,currency,purpose,date,trans type\n" +
                "TR-47884222201,online transfer,-140,USD,donation,2020-02-02,D";


        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CsvParser().parseToList(content));

        Assertions.assertEquals("invalid amount : -140", exception.getMessage());

    }


    @Test
    public void givenValidReader_whenParseToList_thenResultShouldBeAsExpected() throws NoSuchFileException {
        CsvParser parser = new CsvParser();
        String content = new CsvFileReader().getContent(file);


        List<TransactionRecord> records = parser.parseToList(content);

        Assertions.assertEquals(6, records.size(), "size not as expected");

        String reference = "TR-47884222201";
        BigDecimal amount = BigDecimal.valueOf(140);
        String code = "USD";
        LocalDate date = LocalDate.parse("2020-01-20");
        Assertions.assertEquals(reference, records.get(0).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(records.get(0).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, records.get(0).getCode(), "code not as expected");
        Assertions.assertEquals(date, records.get(0).getDate(), "date not as expected");

        reference = "TR-47884222202";
        amount = BigDecimal.valueOf(20.0000);
        code = "JOD";
        date = LocalDate.parse("2020-01-22");
        Assertions.assertEquals(reference, records.get(1).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(records.get(1).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, records.get(1).getCode(), "code not as expected");
        Assertions.assertEquals(date, records.get(1).getDate(), "date not as expected");

        reference = "TR-47884222203";
        amount = BigDecimal.valueOf(5000);
        code = "JOD";
        date = LocalDate.parse("2020-01-25");
        Assertions.assertEquals(reference, records.get(2).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(records.get(2).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, records.get(2).getCode(), "code not as expected");
        Assertions.assertEquals(date, records.get(2).getDate(), "date not as expected");

        reference = "TR-47884222204";
        amount = BigDecimal.valueOf(1200.000);
        code = "JOD";
        date = LocalDate.parse("2020-01-31");
        Assertions.assertEquals(reference, records.get(3).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(records.get(3).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, records.get(3).getCode(), "code not as expected");
        Assertions.assertEquals(date, records.get(3).getDate(), "date not as expected");

        reference = "TR-47884222205";
        amount = BigDecimal.valueOf(60.0);
        code = "JOD";
        date = LocalDate.parse("2020-02-02");
        Assertions.assertEquals(reference, records.get(4).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(records.get(4).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, records.get(4).getCode(), "code not as expected");
        Assertions.assertEquals(date, records.get(4).getDate(), "date not as expected");

        reference = "TR-47884222206";
        amount = BigDecimal.valueOf(500.0);
        code = "USD";
        date = LocalDate.parse("2020-02-10");
        Assertions.assertEquals(reference, records.get(5).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(records.get(5).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, records.get(5).getCode(), "code not as expected");
        Assertions.assertEquals(date, records.get(5).getDate(), "date not as expected");


    }


}
