package com.progressoft.jip9.reconcialiation.parser;

import com.progressoft.jip9.reconcialation.common.entity.AbstractRecord;
import com.progressoft.jip9.reconcialation.common.entity.TransactionRecord;
import com.progressoft.jip9.reconcialiation.reader.JsonFileReader;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;

public class JsonParserTest {

    private final Path path = Paths.get("/home/user/work/induction/jip-9-javase/induction-reconciliation-task/sample-files/input-files/");
    private final Path file = path.resolve("online-banking-transactions.json");
    private final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");


    @Test
    public void givenNullContent_whenParseToList_thenFail() {
        String content = null;
        Parser parser = new JsonParser();

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> parser.parseToList(content));

        Assertions.assertEquals("null data", exception.getMessage());

    }

    @Test
    public void givenEmptyContent_whenParseToList_thenFail() {
        String content = "";
        Parser parser = new JsonParser();

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> parser.parseToList(content));

        Assertions.assertEquals("empty data", exception.getMessage());

    }

    @Test
    public void givenInvalidJsonFormat_whenParseToList_ThenFailed() {
        String content =
                "[" +
                        "{ \"name\":\"Ahmed\", \"age\":32, \"city\":\"Amman\"} ," +
                        "name : Ali , age : 35 , city: Amman" +
                        "]";
        Parser parser = new JsonParser();

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class,
                () -> parser.parseToList(content));

        Assertions.assertEquals("invalid JSON format", exception.getMessage());
    }

    @Test
    public void givenValidJsonAndEmptyReference_whenParseToList_thenFail() {
        String content =
                "[\n" +
                        "  {\n" +
                        "    \"date\": \"15/07/1988\",\n" +
                        "    \"reference\": \"\",\n" +
                        "    \"amount\": \"140.00\",\n" +
                        "    \"currencyCode\": \"USD\",\n" +
                        "    \"purpose\": \"donation\"\n" +
                        "  }" +
                        "]";
        Parser parser = new JsonParser();

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> parser.parseToList(content));

        Assertions.assertEquals("missing reference", exception.getMessage());

    }

    @Test
    public void givenValidJsonAndEmptyCode_whenParseToList_thenFail() {
        String content =
                "[\n" +
                        "  {\n" +
                        "    \"date\": \"20/01/2020\",\n" +
                        "    \"reference\": \"TR-234\",\n" +
                        "    \"amount\": \"140.00\",\n" +
                        "    \"currencyCode\": \"\",\n" +
                        "    \"purpose\": \"donation\"\n" +
                        "  }" +
                        "]";
        Parser parser = new JsonParser();

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> parser.parseToList(content));

        Assertions.assertEquals("missing currency code", exception.getMessage());

    }

    @Test
    public void givenValidJsonAndInvalidCode_whenParseToList_thenFail() {
        String content =
                "[\n" +
                        "  {\n" +
                        "    \"date\": \"20/01/2020\",\n" +
                        "    \"reference\": \"TR-234\",\n" +
                        "    \"amount\": \"140.00\",\n" +
                        "    \"currencyCode\": \"XYZ\",\n" +
                        "    \"purpose\": \"donation\"\n" +
                        "  }" +
                        "]";
        Parser parser = new JsonParser();

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> parser.parseToList(content));

        Assertions.assertEquals("invalid currency", exception.getMessage());

    }

    @Test
    public void givenValidJsonAndEmptyDate_whenParseToList_thenFail() {
        String content =
                "[\n" +
                        "  {\n" +
                        "    \"date\": \"\",\n" +
                        "    \"reference\": \"TR-234\",\n" +
                        "    \"amount\": \"140.00\",\n" +
                        "    \"currencyCode\": \"USD\",\n" +
                        "    \"purpose\": \"donation\"\n" +
                        "  }" +
                        "]";
        Parser parser = new JsonParser();

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> parser.parseToList(content));

        Assertions.assertEquals("missing date", exception.getMessage());

    }

    @Test
    public void givenValidJsonAndEmptyAmount_whenParseToList_thenFail() {
        String content =
                "[\n" +
                        "  {\n" +
                        "    \"date\": \"22/3/2019\",\n" +
                        "    \"reference\": \"TR-234\",\n" +
                        "    \"amount\": \"\",\n" +
                        "    \"currencyCode\": \"USD\",\n" +
                        "    \"purpose\": \"donation\"\n" +
                        "  }" +
                        "]";
        Parser parser = new JsonParser();

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> parser.parseToList(content));

        Assertions.assertEquals("missing amount", exception.getMessage());

    }

    @Test
    public void givenValidJsonAndInvalidAmount_whenParseToList_thenFail() {
        String content =
                "[\n" +
                        "  {\n" +
                        "    \"date\": \"22/3/2019\",\n" +
                        "    \"reference\": \"TR-234\",\n" +
                        "    \"amount\": \"-256\",\n" +
                        "    \"currencyCode\": \"USD\",\n" +
                        "    \"purpose\": \"donation\"\n" +
                        "  }" +
                        "]";
        Parser parser = new JsonParser();

        IllegalArgumentException exception = Assertions.assertThrows(IllegalArgumentException.class, () -> parser.parseToList(content));

        Assertions.assertEquals("invalid amount : -256", exception.getMessage());

    }

    @Test
    public void givenValidContent_whenParseToList_thenResultShouldBeAsExpected() throws NoSuchFileException {
        Parser parser = new JsonParser();
        String content = new JsonFileReader().getContent(file);


        List<TransactionRecord> records = parser.parseToList(content);

        Assertions.assertEquals(7, records.size(), "size not as expected");

        String reference = "TR-47884222201";
        BigDecimal amount = BigDecimal.valueOf(140);
        String code = "USD";
        LocalDate date = LocalDate.parse("20/01/2020", formatter);
        Assertions.assertEquals(reference, records.get(0).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(records.get(0).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, records.get(0).getCode(), "code not as expected");
        Assertions.assertEquals(date, records.get(0).getDate(), "date not as expected");

        reference = "TR-47884222205";
        amount = BigDecimal.valueOf(60.000);
        code = "JOD";
        date = LocalDate.parse("03/02/2020", formatter);
        Assertions.assertEquals(reference, records.get(1).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(records.get(1).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, records.get(1).getCode(), "code not as expected");
        Assertions.assertEquals(date, records.get(1).getDate(), "date not as expected");

        reference = "TR-47884222206";
        amount = BigDecimal.valueOf(500.00);
        code = "USD";
        date = LocalDate.parse("10/02/2020", formatter);
        Assertions.assertEquals(reference, records.get(2).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(records.get(2).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, records.get(2).getCode(), "code not as expected");
        Assertions.assertEquals(date, records.get(2).getDate(), "date not as expected");

        reference = "TR-47884222202";
        amount = BigDecimal.valueOf(30.000);
        code = "JOD";
        date = LocalDate.parse("22/01/2020", formatter);
        Assertions.assertEquals(reference, records.get(3).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(records.get(3).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, records.get(3).getCode(), "code not as expected");
        Assertions.assertEquals(date, records.get(3).getDate(), "date not as expected");

        reference = "TR-47884222217";
        amount = BigDecimal.valueOf(12000.000);
        code = "JOD";
        date = LocalDate.parse("14/02/2020", formatter);
        Assertions.assertEquals(reference, records.get(4).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(records.get(4).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, records.get(4).getCode(), "code not as expected");
        Assertions.assertEquals(date, records.get(4).getDate(), "date not as expected");

        reference = "TR-47884222203";
        amount = BigDecimal.valueOf(5000.000);
        code = "JOD";
        date = LocalDate.parse("25/01/2020", formatter);
        Assertions.assertEquals(reference, records.get(5).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(records.get(5).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, records.get(5).getCode(), "code not as expected");
        Assertions.assertEquals(date, records.get(5).getDate(), "date not as expected");

        reference = "TR-47884222245";
        amount = BigDecimal.valueOf(420.00);
        code = "USD";
        date = LocalDate.parse("12/01/2020", formatter);
        Assertions.assertEquals(reference, records.get(6).getReference(), "reference not as expected");
        Assertions.assertEquals(amount.compareTo(records.get(6).getAmount()), 0, "amount not as expected");
        Assertions.assertEquals(code, records.get(6).getCode(), "code not as expected");
        Assertions.assertEquals(date, records.get(6).getDate(), "date not as expected");

    }


}
