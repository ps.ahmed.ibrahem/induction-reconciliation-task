package com.progressoft.jip9.reconcialiation.reader;

import com.progressoft.jip9.reconcialation.common.entity.FileType;

public class ReaderFactory {
    public static TransactionReader getReader(FileType fileType){
        switch (fileType){
            case CSV: return new CsvFileReader();
            case JSON: return new JsonFileReader();
            default: throw new IllegalArgumentException ("file type not supported");
        }
    }
}
