package com.progressoft.jip9.reconcialiation.reader;

import com.progressoft.jip9.reconcialiation.reader.exceptions.CouldNotReadFileException;
import com.progressoft.jip9.reconcialiation.reader.util.PathValidator;
import org.apache.commons.io.FileUtils;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;

public abstract class TransactionReader {

    public  String getContent(Path path) throws NoSuchFileException {
        PathValidator.validate(path);

        try {
            String content = FileUtils.readFileToString(path.toFile(), StandardCharsets.UTF_8);
            return content;
        } catch (IOException e) {
            throw new CouldNotReadFileException("error while reading", e);
        }
    }

}
