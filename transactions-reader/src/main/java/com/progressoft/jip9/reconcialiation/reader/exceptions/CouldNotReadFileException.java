package com.progressoft.jip9.reconcialiation.reader.exceptions;

import java.io.IOException;

public class CouldNotReadFileException extends RuntimeException {
    public CouldNotReadFileException() {
        super();
    }

    public CouldNotReadFileException(String message) {
        super(message);
    }

    public CouldNotReadFileException(String message, Throwable cause) {
        super(message, cause);
    }

    public CouldNotReadFileException(Throwable cause) {
        super(cause);
    }
}
