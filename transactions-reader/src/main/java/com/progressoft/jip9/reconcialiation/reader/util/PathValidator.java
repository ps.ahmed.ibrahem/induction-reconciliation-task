package com.progressoft.jip9.reconcialiation.reader.util;

import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;

public class PathValidator {
    public static void validate(Path path) throws NoSuchFileException {
        if (path == null) {
            throw new IllegalArgumentException("null path");
        }
        if (Files.isDirectory(path)) {
            throw new NoSuchFileException("file name not provided");
        }
        if (Files.notExists(path)) {
            throw new NoSuchFileException("no such file");
        }
    }
}
