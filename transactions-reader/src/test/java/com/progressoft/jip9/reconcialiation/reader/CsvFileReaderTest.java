package com.progressoft.jip9.reconcialiation.reader;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CsvFileReaderTest {

    @Test
    public void givenNullPath_whenInitiateReader_thenThrowException() {
        Path path = null;

        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new CsvFileReader().getContent(path));

        Assertions.assertEquals("null path", thrown.getMessage());

    }

    @Test
    public void givenDirectoryWithNoFile_whenInitiateReader_thenThrowException() {
        // TODO this failed on my machine, you should use temporary or relative paths
        Path path = Paths.get("/home/user/work/induction/jip-9-javase/induction-reconciliation-task/sample-files/input-files");

        NoSuchFileException thrown = Assertions.assertThrows(NoSuchFileException.class,
                () -> new CsvFileReader().getContent(path));

        Assertions.assertEquals("file name not provided", thrown.getMessage());

    }

    @Test
    public void givenValidDirectoryAndInvalidFile_whenInitiateReader_thenThrowException() {
        Path path = Paths.get("/home/user/work/induction/jip-9-javase/induction-reconciliation-task/sample-files/input-files/");
        Path file = path.resolve("honololo.csv");

        NoSuchFileException thrown = Assertions.assertThrows(NoSuchFileException.class,
                () -> new CsvFileReader().getContent(file));

        Assertions.assertEquals("no such file", thrown.getMessage());

    }

    @Test
    public void givenInvalidFileDirectoryAndValidFile_whenInitiateParser_thenThrowException() {
        Path path = Paths.get("/home/user/");
        Path file = path.resolve("bank-transactions.csv");

        NoSuchFileException thrown = Assertions.assertThrows(NoSuchFileException.class,
                () -> new CsvFileReader().getContent(file));

        Assertions.assertEquals("no such file", thrown.getMessage());
    }

    @Test
    public void givenValidFilePath_whenGetContent_thenResultShouldBeAsExpected() throws NoSuchFileException {
        Path path = Paths.get("/home/user/work/induction/jip-9-javase/induction-reconciliation-task/sample-files/input-files/");
        Path file = path.resolve("bank-transactions.csv");

        TransactionReader reader = new CsvFileReader();
        String expected = "transaction id,trans description,amount,currency,purpose,date,trans type\n" +
                "TR-47884222201,online transfer,140,USD,donation,2020-01-20,D\n" +
                "TR-47884222202,atm withdrwal,20.0000,JOD,,2020-01-22,D\n" +
                "TR-47884222203,counter withdrawal,5000,JOD,,2020-01-25,D\n" +
                "TR-47884222204,salary,1200.000,JOD,donation,2020-01-31,C\n" +
                "TR-47884222205,atm withdrwal,60.0,JOD,,2020-02-02,D\n" +
                "TR-47884222206,atm withdrwal,500.0,USD,,2020-02-10,D";

        String actual = reader.getContent(file);

        Assertions.assertTrue(actual.equals(expected) , "wrong result");

    }
}
