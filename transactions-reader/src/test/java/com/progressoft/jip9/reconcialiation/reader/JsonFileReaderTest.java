package com.progressoft.jip9.reconcialiation.reader;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;

public class JsonFileReaderTest {

    @Test
    public void givenNullPath_whenInitiateReader_thenThrowException() {
        Path path = null;

        IllegalArgumentException thrown = Assertions.assertThrows(IllegalArgumentException.class,
                () -> new JsonFileReader().getContent(path));

        Assertions.assertEquals("null path", thrown.getMessage());

    }

    @Test
    public void givenDirectoryWithNoFile_whenInitiateReader_thenThrowException() {
        Path path = Paths.get("/home/user/work/induction/jip-9-javase/induction-reconciliation-task/sample-files/input-files");

        NoSuchFileException thrown = Assertions.assertThrows(NoSuchFileException.class,
                () -> new JsonFileReader().getContent(path));

        Assertions.assertEquals("file name not provided", thrown.getMessage());

    }

    @Test
    public void givenValidDirectoryAndInvalidFile_whenInitiateReader_thenThrowException() {
        Path path = Paths.get("/home/user/work/induction/jip-9-javase/induction-reconciliation-task/sample-files/input-files/");
        Path file = path.resolve("honololo.json");

        NoSuchFileException thrown = Assertions.assertThrows(NoSuchFileException.class,
                () -> new JsonFileReader().getContent(file));

        Assertions.assertEquals("no such file", thrown.getMessage());

    }

    @Test
    public void givenInvalidFileDirectoryAndValidFile_whenInitiateParser_thenThrowException() {
        Path path = Paths.get("/home/user/");
        Path file = path.resolve("online-banking-transactions.json");

        NoSuchFileException thrown = Assertions.assertThrows(NoSuchFileException.class,
                () -> new JsonFileReader().getContent(file));

        Assertions.assertEquals("no such file", thrown.getMessage());
    }

    @Test
    public void givenValidJsonFilePath_whenGetContent_thenResultShouldBeAsExpected() throws NoSuchFileException {
        Path path = Paths.get("/home/user/work/induction/jip-9-javase/induction-reconciliation-task/sample-files/input-files/");
        Path file = path.resolve("online-banking-transactions.json");

        TransactionReader reader = new JsonFileReader();
        String expected = "[\n" +
                "  {\n" +
                "    \"date\": \"20/01/2020\",\n" +
                "    \"reference\": \"TR-47884222201\",\n" +
                "    \"amount\": \"140.00\",\n" +
                "    \"currencyCode\": \"USD\",\n" +
                "    \"purpose\": \"donation\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"03/02/2020\",\n" +
                "    \"reference\": \"TR-47884222205\",\n" +
                "    \"amount\": \"60.000\",\n" +
                "    \"currencyCode\": \"JOD\",\n" +
                "    \"purpose\": \"\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"10/02/2020\",\n" +
                "    \"reference\": \"TR-47884222206\",\n" +
                "    \"amount\": \"500.00\",\n" +
                "    \"currencyCode\": \"USD\",\n" +
                "    \"purpose\": \"general\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"22/01/2020\",\n" +
                "    \"reference\": \"TR-47884222202\",\n" +
                "    \"amount\": \"30.000\",\n" +
                "    \"currencyCode\": \"JOD\",\n" +
                "    \"purpose\": \"donation\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"14/02/2020\",\n" +
                "    \"reference\": \"TR-47884222217\",\n" +
                "    \"amount\": \"12000.000\",\n" +
                "    \"currencyCode\": \"JOD\",\n" +
                "    \"purpose\": \"salary\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"25/01/2020\",\n" +
                "    \"reference\": \"TR-47884222203\",\n" +
                "    \"amount\": \"5000.000\",\n" +
                "    \"currencyCode\": \"JOD\",\n" +
                "    \"purpose\": \"not specified\"\n" +
                "  },\n" +
                "  {\n" +
                "    \"date\": \"12/01/2020\",\n" +
                "    \"reference\": \"TR-47884222245\",\n" +
                "    \"amount\": \"420.00\",\n" +
                "    \"currencyCode\": \"USD\",\n" +
                "    \"purpose\": \"loan\"\n" +
                "  }\n" +
                "]";

        String actual = reader.getContent(file);

        Assertions.assertEquals(actual, expected, "wrong result");

    }


}
