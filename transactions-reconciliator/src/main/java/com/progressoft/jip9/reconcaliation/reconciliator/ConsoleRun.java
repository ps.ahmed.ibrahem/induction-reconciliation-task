package com.progressoft.jip9.reconcaliation.reconciliator;

import com.progressoft.jip9.reconcialation.common.entity.FileType;
import com.progressoft.jip9.reconcialation.common.entity.ReconciliationRequest;

import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class ConsoleRun {

    public static void main(String[] args) throws NoSuchFileException {
        Scanner input = new Scanner(System.in);

        System.out.println("enter the source file path : ");
        Path sourceUrl = Paths.get(input.nextLine().trim());
        System.out.println("enter the source file type : ");
        FileType sourceType = getFileType(input.nextLine().trim());


        System.out.println("enter the target file path : ");
        Path targetUrl = Paths.get(input.nextLine().trim());
        System.out.println("enter the target file type : ");
        FileType targetType = getFileType(input.nextLine().trim());


        ReconciliationRequest request = new ReconciliationRequest(sourceUrl, sourceType, targetUrl, targetType);
        new ReconcilingFacade().reconcile(request);
    }

    private static FileType getFileType(String srcType) {
        if (srcType.equalsIgnoreCase("CSV")) {
            return FileType.CSV;
        } else if (srcType.equalsIgnoreCase("JSON")) {
            return FileType.JSON;
        } else {
            throw new IllegalArgumentException("unsupported file type");
        }
    }
}
