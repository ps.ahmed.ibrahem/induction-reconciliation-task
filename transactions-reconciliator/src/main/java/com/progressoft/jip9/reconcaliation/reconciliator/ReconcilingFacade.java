package com.progressoft.jip9.reconcaliation.reconciliator;

import com.progressoft.jip9.reconcialation.common.entity.ComparisonResult;
import com.progressoft.jip9.reconcialation.common.entity.FileType;
import com.progressoft.jip9.reconcialation.common.entity.ReconciliationRequest;
import com.progressoft.jip9.reconcialation.common.entity.TransactionRecord;
import com.progressoft.jip9.reconcialiation.parser.Parser;
import com.progressoft.jip9.reconcialiation.parser.ParserFactory;
import com.progressoft.jip9.reconcialiation.reader.ReaderFactory;
import com.progressoft.jip9.reconcialiation.reader.TransactionReader;
import com.progressoft.jip9.reconciliation.comparator.DefaultComparator;
import com.progressoft.jip9.reconciliation.exporter.CsvExporter;

import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ReconcilingFacade {
    private final Path exportPath = Paths.get("/home/user/result-files/");

    public void reconcile(ReconciliationRequest request) throws NoSuchFileException {
        validateRequest(request);

        Path sourceUrl = request.getSourceUrl();
        Path targetUrl = request.getTargetUrl();
        FileType sourceType = request.getSourceType();
        FileType targetType = request.getTargetType();

        TransactionReader sourceReader = ReaderFactory.getReader(sourceType);
        TransactionReader targetReader = ReaderFactory.getReader(targetType);
        Parser sourceParser = ParserFactory.getParser(sourceType);
        Parser targetParser = ParserFactory.getParser(targetType);

        List<TransactionRecord> sourceRecords = sourceParser.parseToList(sourceReader.getContent(sourceUrl));
        List<TransactionRecord> targetRecords = targetParser.parseToList(targetReader.getContent(targetUrl));

        ComparisonResult comparisonResult = new DefaultComparator().compare(sourceRecords,targetRecords);
        new CsvExporter().export(exportPath , comparisonResult);


    }

    private void validateRequest(ReconciliationRequest request) {
        if (request == null) {
            throw new IllegalArgumentException("null request");
        }
        if ( request.getSourceUrl() == null) {
            throw new IllegalArgumentException("source url not provided");
        }
        if ( request.getTargetUrl() == null) {
            throw new IllegalArgumentException("target url not provided");
        }
    }
}
