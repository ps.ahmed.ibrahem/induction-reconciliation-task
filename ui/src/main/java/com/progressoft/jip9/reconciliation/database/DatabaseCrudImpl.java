package com.progressoft.jip9.reconciliation.database;

import com.progressoft.jip9.reconciliation.model.util.DataSourceFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class DatabaseCrudImpl implements DataBaseCrud {

    private final Connection connection;

    public DatabaseCrudImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void createTables() {
        try {
            createUserTable();
            createActivityTable();
            truncateUserTable();
            truncateActivityTable();
        } finally {
            try {
                connection.close();
            } catch (SQLException throwables) {
                throw new RuntimeException("could not close connection");
            }
        }

    }

    private void truncateUserTable(){
        String query = "DELETE FROM `reconciliation`.`users` WHERE id > 0" ;

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();
        } catch (SQLException throwables) {
            throw new RuntimeException("could not truncate user table" ,throwables);
        }
    }
    private void truncateActivityTable(){
        String query = "TRUNCATE TABLE `reconciliation`.`activities` ";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();
        } catch (SQLException throwables) {
            throw new RuntimeException("could not truncate activity table" ,throwables);
        }
    }


    private void createUserTable() {
        String query = "CREATE TABLE IF NOT EXISTS `reconciliation`.`users` (\n" +
                "  `id` BIGINT NOT NULL AUTO_INCREMENT,\n" +
                "  `user_name` VARCHAR(250) NOT NULL,\n" +
                "  `password` VARCHAR(250) NOT NULL,\n" +
                "  PRIMARY KEY (`id`),\n" +
                "  UNIQUE INDEX `user_name_UNIQUE` (`user_name` ASC),\n" +
                "  UNIQUE INDEX `password_UNIQUE` (`password` ASC)) ; ";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();
        } catch (SQLException throwables) {
            throw new RuntimeException("could not create user table" ,throwables);
        }

    }

    private void createActivityTable() {
        String query = "CREATE TABLE IF NOT EXISTS `reconciliation`.`activities` (\n" +
                "  `id` BIGINT NOT NULL AUTO_INCREMENT,\n" +
                "  `sessionTime` VARCHAR(100) NOT NULL,\n" +
                "  `reconTime` VARCHAR(250) NOT NULL,\n" +
                "  `srcFileName` VARCHAR(150) NOT NULL,\n" +
                "  `trgtFileName` VARCHAR(150) NOT NULL,\n" +
                "  `matched` INT NOT NULL,\n" +
                "  `mismatched` INT NOT NULL,\n" +
                "  `missing` INT NOT NULL,\n" +
                "  `userName` VARCHAR(200) NOT NULL,\n" +
                "  PRIMARY KEY (`id`),\n" +
                "  INDEX `fk_activities_users_idx` (`userName` ASC),\n" +
                "  CONSTRAINT `fk_activities_users`\n" +
                "    FOREIGN KEY (`userName`)\n" +
                "    REFERENCES `reconciliation`.`users` (`user_name`)) ;";

        try (PreparedStatement statement = connection.prepareStatement(query)) {
            statement.execute();
        } catch (SQLException throwables) {
            throw new RuntimeException("could not create activity table" , throwables);
        }
    }

    public static void main(String[] args) {
        DatabaseCrudImpl databaseCrud = new DatabaseCrudImpl(DataSourceFactory.getConnection());
        databaseCrud.createTables();
    }


}
