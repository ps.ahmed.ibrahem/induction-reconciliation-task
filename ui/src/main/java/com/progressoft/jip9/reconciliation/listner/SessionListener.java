package com.progressoft.jip9.reconciliation.listner;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.io.File;

@WebListener ()
public class SessionListener implements HttpSessionListener {
    @Override
    public void sessionCreated(HttpSessionEvent se) {
        System.out.println("session created " + se.getSession().getId());
    }

    @Override
    public void sessionDestroyed(HttpSessionEvent se) {
        String uploadDir = se.getSession().getServletContext().getInitParameter("fileUploadDir");

        new File(uploadDir, se.getSession().getId() + "_source").delete();
        new File(uploadDir, se.getSession().getId() + "_target").delete();
        System.out.println("session destroyed " + se.getSession().getId());

    }
}
