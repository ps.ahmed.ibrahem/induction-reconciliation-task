package com.progressoft.jip9.reconciliation.model.activites.dao;

import com.progressoft.jip9.reconciliation.model.activites.entity.Activity;

import java.util.List;

public interface ActivityDao {
   List< Activity> getActivitiesByUserName(String name);
    void addActivity(Activity activity);
}
