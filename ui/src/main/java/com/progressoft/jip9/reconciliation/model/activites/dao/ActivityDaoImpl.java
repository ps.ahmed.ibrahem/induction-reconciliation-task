package com.progressoft.jip9.reconciliation.model.activites.dao;

import com.progressoft.jip9.reconciliation.model.activites.entity.Activity;
import com.progressoft.jip9.reconciliation.model.util.DataSourceFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ActivityDaoImpl implements ActivityDao {

    @Override
    public List<Activity> getActivitiesByUserName(String name) {
        String query = "SELECT * FROM activities where activities.userName = ? ; ";
        try (Connection connection = DataSourceFactory.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, name);
            try {

                ResultSet resultSet = preparedStatement.executeQuery();
                List<Activity> activities = new ArrayList<>();
                while (resultSet.next()) {
                    String sessionTime = resultSet.getString(2);
                    String reconTime = resultSet.getString(3);
                    String srcFileName = resultSet.getString(4);
                    String trgtFileName = resultSet.getString(5);
                    int matched = resultSet.getInt(6);
                    int mismatched = resultSet.getInt(7);
                    int missing = resultSet.getInt(8);
                    String userName = resultSet.getString(9);
                    Activity activity = new Activity(sessionTime, reconTime, srcFileName, trgtFileName
                            , matched, mismatched, missing, userName);
                    activities.add(activity);
                }

                return activities;

            } catch (SQLException throwables) {
                // TODO duplicate exception handlng and why the real cause is not attached
                throw new RuntimeException("could not execute query");
            }

        } catch (SQLException e) {
            throw new RuntimeException("could not get connection");
        }
    }

    @Override
    public void addActivity(Activity activity) {
        String query = "INSERT INTO activities VALUES (default ,?,?,?,?,?,?,?,?) ;";
        try (Connection connection = DataSourceFactory.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, activity.getSessionTime());
            preparedStatement.setString(2, activity.getReconTime());
            preparedStatement.setString(3, activity.getSrcFileName());
            preparedStatement.setString(4, activity.getTrgtFileName());
            preparedStatement.setInt(5, activity.getMatched());
            preparedStatement.setInt(6, activity.getMismatched());
            preparedStatement.setInt(7, activity.getMissing());
            preparedStatement.setString(8, activity.getUserName());

            try {
                preparedStatement.execute();
            } catch (SQLException throwables) {
                throw new RuntimeException("could not execute query");
            }

        } catch (SQLException e) {
            throw new RuntimeException("could not get connection");
        }

    }
}
