package com.progressoft.jip9.reconciliation.model.activites.entity;

public class Activity {
    private String sessionTime ;
    private String reconTime;
    private String srcFileName ;
    private String trgtFileName ;
    private int matched ;
    private int mismatched ;
    private int missing ;
    private String userName ;

    public Activity(String sessionTime, String reconTime, String srcFileName, String trgtFileName, int matched, int mismatched, int missing ,String userName) {
        this.sessionTime = sessionTime;
        this.reconTime = reconTime;
        this.srcFileName = srcFileName;
        this.trgtFileName = trgtFileName;
        this.matched = matched;
        this.mismatched = mismatched;
        this.missing = missing;
        this.userName = userName ;
    }

    public String getSessionTime() {
        return sessionTime;
    }

    public void setSessionTime(String sessionTime) {
        this.sessionTime = sessionTime;
    }

    public String getReconTime() {
        return reconTime;
    }

    public void setReconTime(String reconTime) {
        this.reconTime = reconTime;
    }

    public String getSrcFileName() {
        return srcFileName;
    }

    public void setSrcFileName(String srcFileName) {
        this.srcFileName = srcFileName;
    }

    public String getTrgtFileName() {
        return trgtFileName;
    }

    public void setTrgtFileName(String trgtFileName) {
        this.trgtFileName = trgtFileName;
    }

    public int getMatched() {
        return matched;
    }

    public void setMatched(int matched) {
        this.matched = matched;
    }

    public int getMismatched() {
        return mismatched;
    }

    public void setMismatched(int mismatched) {
        this.mismatched = mismatched;
    }

    public int getMissing() {
        return missing;
    }

    public void setMissing(int missing) {
        this.missing = missing;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
