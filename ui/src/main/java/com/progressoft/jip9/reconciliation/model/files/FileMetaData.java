package com.progressoft.jip9.reconciliation.model.files;

import java.util.Objects;

public class FileMetaData {
    private String fileName ;
    private String fileType ;
    private String fileFullName ;

    public FileMetaData(String fileName, String fileType, String fileFullName) {
        this.fileName = fileName;
        this.fileType = fileType;
        this.fileFullName = fileFullName;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileType() {
        return fileType;
    }

    public String getFileFullName() {
        return fileFullName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FileMetaData metaData = (FileMetaData) o;
        return Objects.equals(fileName, metaData.fileName) &&
                Objects.equals(fileType, metaData.fileType) &&
                Objects.equals(fileFullName, metaData.fileFullName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fileName, fileType, fileFullName);
    }
}
