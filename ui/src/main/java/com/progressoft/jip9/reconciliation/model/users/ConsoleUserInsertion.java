package com.progressoft.jip9.reconciliation.model.users;

import com.progressoft.jip9.reconciliation.model.users.dao.UserDao;
import com.progressoft.jip9.reconciliation.model.users.dao.UserDaoImpl;
import com.progressoft.jip9.reconciliation.model.users.entity.User;

public class ConsoleUserInsertion {
    public static void main(String[] args) {
        UserDao userDao = new UserDaoImpl();
        User user1 = new User("ahmed@ps.com" , "ahmed");
        User user2 = new User("ali@ps.com", "ali");
        User user3 = new User("saleh@ps.com" , "saleh");

        userDao.addUser(user1);
        userDao.addUser(user2);
        userDao.addUser(user3);
    }
}
