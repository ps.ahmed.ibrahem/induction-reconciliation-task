package com.progressoft.jip9.reconciliation.model.users.dao;

import com.progressoft.jip9.reconciliation.model.users.entity.User;

public interface UserDao {
    void addUser(User user);
    User getByName(String name);
}
