package com.progressoft.jip9.reconciliation.model.users.dao;

import com.progressoft.jip9.reconciliation.model.users.entity.User;
import com.progressoft.jip9.reconciliation.model.util.DataSourceFactory;
import com.progressoft.jip9.reconciliation.model.util.PasswordHashing;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserDaoImpl implements UserDao {
    @Override
    public void addUser(User user) {
        String query = "INSERT INTO users VALUES (default,?,?) ;";
        try (Connection connection = DataSourceFactory.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, user.getUserName());
            String hashedPassword = PasswordHashing.getHashed(user.getPassword());
            preparedStatement.setString(2, hashedPassword);
            try {
                preparedStatement.execute();
            } catch (SQLException throwables) {
                throw new RuntimeException("could not add user" ,throwables);
            }

        } catch (SQLException e) {
            throw new RuntimeException("could not get connection" ,e);
        }


    }

    @Override
    public User getByName(String name) {
        String query = "SELECT * FROM users where users.user_name = ? ; ";
        try (Connection connection = DataSourceFactory.getConnection();
             PreparedStatement preparedStatement = connection.prepareStatement(query)) {
            preparedStatement.setString(1, name);
            try {
                ResultSet resultSet = preparedStatement.executeQuery();
                if (!resultSet.next()){
                    return null ;
                }
                String userName = resultSet.getString(2);
                String password = resultSet.getString(3);
                return new User(userName, password);
            } catch (SQLException throwables) {
                throw new SQLException("could not execute query : " + query , throwables);
            }

        } catch (SQLException e) {
            throw new RuntimeException("could not get connection");
        }
    }

    public static void main(String[] args) {
        UserDao userDao = new UserDaoImpl();
        User byName = userDao.getByName("Ahmed@psd.com");
        System.out.println(byName.getPassword());
    }

}
