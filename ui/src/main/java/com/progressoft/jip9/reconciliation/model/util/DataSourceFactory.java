package com.progressoft.jip9.reconciliation.model.util;

import com.mysql.cj.jdbc.MysqlDataSource;
import org.h2.jdbcx.JdbcDataSource;

;import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

public class DataSourceFactory {




    public static Connection getConnection()  {

        String dbType =getConfigFile().getProperty("db.engine");

        switch (dbType) {
            case "mysql":
                return getMysqlConnection();
            case "h2":
                return getH2Connection();
            default:
                throw new IllegalArgumentException("no such DB");
        }
    }

    private static Properties getConfigFile(){
        InputStream inputStream = DataSourceFactory.class.getClassLoader().getResourceAsStream("config.properties");
        Properties properties = new Properties();
        try {
            properties.load(inputStream);
            return properties;
        } catch (IOException e) {
            throw new RuntimeException("could not load properties" , e);
        }
    }

    private static Connection getMysqlConnection() {
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setURL(getConfigFile().getProperty("db.mysql.url"));
        dataSource.setUser(getConfigFile().getProperty("db.mysql.username"));
        dataSource.setPassword(getConfigFile().getProperty("db.mysql.password"));

        try {
            return dataSource.getConnection();
        } catch (SQLException throwables) {
            throw new RuntimeException("could not get connection", throwables);
        }
    }

    private static Connection getH2Connection() {
        JdbcDataSource dataSource = new JdbcDataSource();
        dataSource.setURL(getConfigFile().getProperty("db.h2.url"));
        try {
            return dataSource.getConnection();
        } catch (SQLException throwables) {
            throw new RuntimeException("could not get H2 Connection", throwables);
        }

    }
}
