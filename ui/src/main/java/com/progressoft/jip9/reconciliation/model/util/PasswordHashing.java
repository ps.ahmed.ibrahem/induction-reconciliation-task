package com.progressoft.jip9.reconciliation.model.util;

import org.mindrot.jbcrypt.BCrypt;


public class PasswordHashing {

    public static String getHashed(String password) {
        return  BCrypt.hashpw(password , BCrypt.gensalt());

    }

    public static void main(String[] args) {

        String sss = PasswordHashing.getHashed("sss");
        System.out.println(sss);
        System.out.println(BCrypt.checkpw("sss" , "$2a$10$osDt7yG2JmVMYsY/Ft9jKeI8E3.z3kwcdKSndGTdsYZLu1Bd2bmU2"));

    }

}
