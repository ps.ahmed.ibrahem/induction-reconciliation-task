package com.progressoft.jip9.reconciliation.servlet;

import com.progressoft.jip9.reconciliation.model.activites.dao.ActivityDao;
import com.progressoft.jip9.reconciliation.model.activites.entity.Activity;
import com.progressoft.jip9.reconciliation.model.users.entity.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ActivityServlet extends HttpServlet {
     private final ActivityDao activityDao ;

    public ActivityServlet(ActivityDao activityDao) {
        this.activityDao = activityDao;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        User user = (User) req.getSession().getAttribute("user");
        String userName = user.getUserName();

        List<Activity> activities = activityDao.getActivitiesByUserName(userName);

        req.getSession().setAttribute("activities" , activities);

        req.getRequestDispatcher("/WEB-INF/views/activities.jsp").forward(req,resp);
    }
}
