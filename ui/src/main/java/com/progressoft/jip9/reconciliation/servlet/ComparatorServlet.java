package com.progressoft.jip9.reconciliation.servlet;

import com.progressoft.jip9.reconcaliation.reconciliator.ReconcilingFacade;
import com.progressoft.jip9.reconcialation.common.entity.*;
import com.progressoft.jip9.reconciliation.model.activites.dao.ActivityDao;
import com.progressoft.jip9.reconciliation.model.activites.entity.Activity;
import com.progressoft.jip9.reconciliation.model.files.FileMetaData;
import com.progressoft.jip9.reconciliation.model.users.entity.User;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;


public class ComparatorServlet extends HttpServlet {
    private final ReconcilingFacade reconcilingFacade;
    private final ActivityDao activityDao;


    public ComparatorServlet(ReconcilingFacade reconcilingFacade, ActivityDao activityDao) {
        this.reconcilingFacade = reconcilingFacade;
        this.activityDao = activityDao;

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        FileMetaData sourceFileMetaData = (FileMetaData) req.getSession().getAttribute("sourceFileMetaData");
        FileMetaData targetFileMetaData = (FileMetaData) req.getSession().getAttribute("targetFileMetaData");
        String uploadDir = getServletContext().getInitParameter("fileUploadDir");


        Path sourcePath = Paths.get(uploadDir, (String) req.getSession().getAttribute("storedSourceFile"));
        Path targetPath = Paths.get(uploadDir, (String) req.getSession().getAttribute("storedTargetFile"));

        FileType sourceType = FileType.valueOf(sourceFileMetaData.getFileType());
        FileType targetType = FileType.valueOf(targetFileMetaData.getFileType());

        ReconciliationRequest reconciliationRequest =
                new ReconciliationRequest(sourcePath, sourceType, targetPath, targetType);
// TODO this is not compiling, it seems match method is not committed
        ComparisonResult result = reconcilingFacade.match(reconciliationRequest);
        req.getSession().setAttribute("result", result);
        activityDao.addActivity(getActivity(req, result));
        req.getRequestDispatcher("/WEB-INF/views/result.jsp").forward(req, resp);
    }

    private Activity getActivity(HttpServletRequest req, ComparisonResult result) {
        FileMetaData sourceFileMetaData = (FileMetaData) req.getSession().getAttribute("sourceFileMetaData");
        FileMetaData targetFileMetaData = (FileMetaData) req.getSession().getAttribute("targetFileMetaData");
        String sessionTime = req.getSession().getAttribute("sessionTime").toString();
        String reconTime = LocalDateTime.now().toString();
        String srcFileName = sourceFileMetaData.getFileFullName();
        String trgtFileName = targetFileMetaData.getFileFullName();
        int matched = result.getMatched().size();
        int mismatched = result.getMismatched().size();
        int missing = result.getMissing().size();
        User user = (User) req.getSession().getAttribute("user");
        String userName = user.getUserName();

        return new Activity(sessionTime, reconTime, srcFileName, trgtFileName
                , matched, mismatched, missing, userName);
    }
}
