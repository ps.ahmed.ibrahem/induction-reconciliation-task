package com.progressoft.jip9.reconciliation.servlet;

import com.progressoft.jip9.reconcialation.common.entity.ComparisonResult;
import com.progressoft.jip9.reconcialation.common.entity.FileType;
import com.progressoft.jip9.reconciliation.exporter.Exporter;
import com.progressoft.jip9.reconciliation.exporter.ExporterFactory;
import com.progressoft.jip9.reconciliation.servlet.util.FileDownloader;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@WebServlet(urlPatterns = {"/in/download"})
public class FileDownloadServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uploadDir = req.getServletContext().getInitParameter("uploadDir");
        String exportPath = uploadDir + req.getSession().getId();

        String downloadType = req.getParameter("downloadAs");
        System.out.println(downloadType);
        FileType fileType = FileType.valueOf(downloadType);
        ComparisonResult result = (ComparisonResult) req.getSession().getAttribute("result");
        Exporter exporter = ExporterFactory.getExporter(fileType);
        exporter.export(Paths.get(exportPath), result);

        File directory = new File(exportPath);
        String[] files = directory.list();

        if (files != null && files.length > 0) {
            byte[] zip = zipFiles(directory, files);
            ServletOutputStream servletOutputStream = resp.getOutputStream();
            resp.setContentType("application/zip");
            resp.setHeader("Content-Disposition", "attachment; filename=results.zip");

            servletOutputStream.write(zip);
            servletOutputStream.flush();

        }
    }


    private byte[] zipFiles(File directory, String[] files) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        ZipOutputStream zipOutputStream = new ZipOutputStream(byteArrayOutputStream);
        byte bytes[] = new byte[2048];

        for (String fileName : files) {
            FileInputStream fileInputStream = new FileInputStream(directory.getPath() +
                    System.getProperty("file.separator") + fileName);
            BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);

            zipOutputStream.putNextEntry(new ZipEntry(fileName));

            int bytesRead;
            while ((bytesRead = bufferedInputStream.read(bytes)) != -1) {
                zipOutputStream.write(bytes, 0, bytesRead);
            }
            zipOutputStream.closeEntry();
            bufferedInputStream.close();
            fileInputStream.close();
        }
        zipOutputStream.flush();
        byteArrayOutputStream.flush();
        zipOutputStream.close();
        byteArrayOutputStream.close();

        return byteArrayOutputStream.toByteArray();
    }
}

