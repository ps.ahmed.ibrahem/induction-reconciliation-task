package com.progressoft.jip9.reconciliation.servlet;

import com.progressoft.jip9.reconciliation.model.users.dao.UserDao;
import com.progressoft.jip9.reconciliation.model.users.entity.User;
import org.mindrot.jbcrypt.BCrypt;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.LocalDateTime;

public class LoginServlet extends HttpServlet {

    private final UserDao userDao;

    public LoginServlet(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String userName = req.getParameter("userName");
        String password = req.getParameter("password");

        User user = userDao.getByName(userName);

        if (!isValidUser(userName, password, user)) {
            req.setAttribute("message", "invalid user name / password");
            req.getRequestDispatcher("/WEB-INF/welcome.jsp").forward(req,resp);
        }
        else {
            req.getSession().setAttribute("user", user);
            req.getSession().setAttribute("sessionTime" , LocalDateTime.now());
            req.setAttribute("message", "");

            resp.sendRedirect(req.getContextPath() + "/in/sourceFile");
        }
    }

    private boolean isValidUser(String userName, String password, User user) {
        return user != null && userName.equalsIgnoreCase(user.getUserName()) &&
                BCrypt.checkpw(password , user.getPassword());
    }
}
