package com.progressoft.jip9.reconciliation.servlet;

import com.progressoft.jip9.reconciliation.model.files.FileMetaData;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

@MultipartConfig
public class SourceFileUploadServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uploadDir = req.getServletContext().getInitParameter("uploadDir");

        String sourceFileName = req.getParameter("sourceFileName");
        String sourceFileType = req.getParameter("sourceFileType");

        Part srcPart = req.getPart("sourceFile");
        String sourceFileFullName = srcPart.getSubmittedFileName();
        String storedFileName = req.getSession().getId().concat("_source");

        File srcFile = new File(uploadDir, storedFileName);
        try (InputStream input = srcPart.getInputStream()) {
            Files.copy(input, srcFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }


        FileMetaData sourceFileMetaData = new FileMetaData(sourceFileName, sourceFileType, sourceFileFullName);
        req.getSession().setAttribute("sourceFileMetaData", sourceFileMetaData);
        req.getSession().setAttribute("storedSourceFile", storedFileName);

        req.getRequestDispatcher("/WEB-INF/views/targetFile.jsp").forward(req, resp);
    }
}
