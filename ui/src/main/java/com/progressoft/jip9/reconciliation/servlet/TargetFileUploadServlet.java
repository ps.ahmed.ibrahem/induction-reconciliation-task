package com.progressoft.jip9.reconciliation.servlet;

import com.progressoft.jip9.reconciliation.model.files.FileMetaData;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

@MultipartConfig
public class TargetFileUploadServlet extends HttpServlet {
    // TODO source and target servlet could looks the same code
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String uploadDir = req.getServletContext().getInitParameter("uploadDir");

        String targetFileName = req.getParameter("targetFileName");
        String targetFileType = req.getParameter("targetFileType");

        Part targetPart = req.getPart("targetFile");
        String sourceFileFullName = targetPart.getSubmittedFileName();
        String storedFileName = req.getSession().getId().concat("_target");

        File srcFile = new File(uploadDir, storedFileName);
        try (InputStream input = targetPart.getInputStream()) {
            Files.copy(input, srcFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }

        FileMetaData targetFileMetaData = new FileMetaData(targetFileName, targetFileType, sourceFileFullName);
        req.getSession().setAttribute("targetFileMetaData", targetFileMetaData);
        req.getSession().setAttribute("storedTargetFile", storedFileName);

        req.getRequestDispatcher("/WEB-INF/views/filesSummary.jsp").forward(req, resp);
    }


}
