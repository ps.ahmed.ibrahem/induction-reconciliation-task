package com.progressoft.jip9.reconciliation.servlet.util;

import javax.servlet.http.HttpServletResponse;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class FileDownloader {

    public static  void download (String fileUrl , HttpServletResponse resp) throws IOException {
        try (InputStream in = new FileInputStream(fileUrl);
             OutputStream out = resp.getOutputStream()) {

            byte[] buffer = new byte[1048];

            int numBytesRead;
            while ((numBytesRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, numBytesRead);
            }
            out.flush();

        }

    }
}
