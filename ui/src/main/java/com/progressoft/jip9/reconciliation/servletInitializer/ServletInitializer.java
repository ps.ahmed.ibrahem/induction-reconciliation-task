package com.progressoft.jip9.reconciliation.servletInitializer;

import com.progressoft.jip9.reconcaliation.reconciliator.ReconcilingFacade;
import com.progressoft.jip9.reconciliation.model.activites.dao.ActivityDao;
import com.progressoft.jip9.reconciliation.model.activites.dao.ActivityDaoImpl;
import com.progressoft.jip9.reconciliation.model.users.dao.UserDao;
import com.progressoft.jip9.reconciliation.model.users.dao.UserDaoImpl;
import com.progressoft.jip9.reconciliation.servlet.*;

import javax.servlet.*;
import java.util.Set;

public class ServletInitializer implements ServletContainerInitializer {

    @Override
    public void onStartup(Set<Class<?>> set, ServletContext servletContext) throws ServletException {
        UserDao userDao = new UserDaoImpl();
        ReconcilingFacade reconcilingFacade = new ReconcilingFacade();
        ActivityDao activityDao = new ActivityDaoImpl();

        registerSourceFileUploadServlet(servletContext);
        registerUserValidationServlet(servletContext ,userDao);
        registerSourceFileServlet(servletContext);
        registerFilesSummaryServlet(servletContext);
        registerTargetFileServlet(servletContext);
        registerTargetFileUploadServlet(servletContext);
        registerComparatorServlet(servletContext , reconcilingFacade , activityDao);
        registerActivityServlet(servletContext , activityDao);
    }


    private void registerActivityServlet(ServletContext servletContext, ActivityDao activityDao) {
        ActivityServlet activityServlet = new ActivityServlet(activityDao);
        servletContext.addServlet("activityServlet" , activityServlet).
                addMapping("/in/activity");
    }

    private void registerComparatorServlet(ServletContext servletContext, ReconcilingFacade reconcilingFacade , ActivityDao activityDao) {
        ComparatorServlet comparatorServlet = new ComparatorServlet( reconcilingFacade , activityDao);
        servletContext.addServlet("comparatorServlet" , comparatorServlet)
                .addMapping("/in/compare");
    }

    private void registerTargetFileServlet(ServletContext servletContext) {
        TargetFileServlet targetFileServlet = new TargetFileServlet();
        servletContext.addServlet("targetFile" , targetFileServlet)
                .addMapping("/in/targetFile");
    }

    private void registerFilesSummaryServlet(ServletContext servletContext) {
        FileSummaryServlet summaryServlet = new FileSummaryServlet();
        servletContext.addServlet("filesSummaryServlet" , summaryServlet)
                .addMapping("/in/filesSummary");
    }

    private void registerSourceFileServlet(ServletContext servletContext) {
        SourceFileServlet sourceFileServlet = new SourceFileServlet();
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("sourceFile", sourceFileServlet);
        servletRegistration.addMapping("/in/sourceFile");

    }

    private void registerUserValidationServlet(ServletContext servletContext, UserDao userDao) {
        LoginServlet loginServlet = new LoginServlet(userDao);
        ServletRegistration.Dynamic servletRegistration = servletContext.addServlet("userValidator", loginServlet);
        servletRegistration.addMapping("/doLogin");
    }

    private void registerSourceFileUploadServlet(ServletContext servletContext) {
        SourceFileUploadServlet sourceFileUploadServlet = new SourceFileUploadServlet();
        ServletRegistration.Dynamic servletRegistration =
                servletContext.addServlet("sourceFileUpload", sourceFileUploadServlet);

        servletRegistration.setMultipartConfig(
                new MultipartConfigElement
                        ("uploads", 1048576,
                                1048576, 262144));

        servletRegistration.addMapping("/in/sourceFileUpload");
    }
    private void registerTargetFileUploadServlet(ServletContext servletContext) {
        TargetFileUploadServlet targetFileUploadServlet = new TargetFileUploadServlet();
        ServletRegistration.Dynamic servletRegistration =
                servletContext.addServlet("/in/targetFileUpload", targetFileUploadServlet);

        servletRegistration.setMultipartConfig(
                new MultipartConfigElement
                        ("uploads", 1048576,
                                1048576, 262144));

        servletRegistration.addMapping("/in/targetFileUpload");
    }

}
