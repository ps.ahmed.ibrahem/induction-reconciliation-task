<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 9/8/20
  Time: 11:29 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Results</title>
    <style>
        .tab {
            overflow: hidden;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }

        .tab button {
            background-color: inherit;
            float: left;
            border: none;
            outline: none;
            cursor: pointer;
            padding: 14px 16px;
            transition: 0.3s;
        }

        .tab button:hover {
            background-color: #ddd;
        }

        .tab button.active {
            background-color: #ccc;
        }

        .tabcontent {
            display: none;
            padding: 6px 12px;
            border: 1px solid #ccc;
            border-top: none;
        }
    </style>
    <script>
        function getTable(evt, cityName) {
            var i, tabcontent, tablinks;

            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }

            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
    </script>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-7 mx-auto">
            <br><h5 align="center">your results</h5><br>
            <div class="tab">
                <button class="tablinks" onclick="getTable(event, 'matchedRecords')">Matched records</button>
                <button class="tablinks" onclick="getTable(event, 'mismatchedRecords')">Mismatched records</button>
                <button class="tablinks" onclick="getTable(event, 'missingRecords')">Missing records</button>
            </div>

            <div id="matchedRecords" class="tabcontent">
                <table class="table table-hover" >
                    <thead>
                    <tr>
                        <th scope="col">transaction id</th>
                        <th scope="col">amount</th>
                        <th scope="col">code</th>
                        <th scope="col">date</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${sessionScope.result.matched}" var="macthed">
                        <tr>
                            <td>${macthed.reference}</td>
                            <td>${macthed.amount}</td>
                            <td>${macthed.code}</td>
                            <td>${macthed.date}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

            <div id="mismatchedRecords" class="tabcontent">
                <table class="table table-hover" >
                    <thead>
                    <tr>
                        <th scope="col">found in file</th>
                        <th scope="col">transaction id</th>
                        <th scope="col">amount</th>
                        <th scope="col">code</th>
                        <th scope="col">date</th>

                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${sessionScope.result.mismatched}" var="mismatched">
                        <tr>
                            <td>${mismatched.destination}</td>
                            <td>${mismatched.reference}</td>
                            <td>${mismatched.amount}</td>
                            <td>${mismatched.code}</td>
                            <td>${mismatched.date}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

            <div id="missingRecords" class="tabcontent">
                <table class="table table-hover" >
                    <thead>
                    <tr>
                        <th scope="col">found in file</th>
                        <th scope="col">transaction id</th>
                        <th scope="col">amount</th>
                        <th scope="col">code</th>
                        <th scope="col">date</th>

                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${sessionScope.result.missing}" var="missing">
                        <tr>
                            <td>${missing.destination}</td>
                            <td>${missing.reference}</td>
                            <td>${missing.amount}</td>
                            <td>${missing.code}</td>
                            <td>${missing.date}</td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

            <div class="input-group mb-auto">
                <div class="input-group-btn">
                    <label class="input-group-text" >Save as</label>
                </div>
                <form method="get" action="${pageContext.request.contextPath}/in/download">
            <select style="background-color: #dddddd" class="custom-select" name="downloadAs" onchange="form.submit()">
                <option value="CSV" selected>CSV</option>
                <option VALUE="JSON" > JSON</option>
            </select>
                </form>
            </div>
<%--            <script type="text/javascript">--%>
<%--                function handleSelect(selector)--%>
<%--                {--%>

<%--                    window.location = "${pageContext.request.contextPath}/in/download?downloadAs="+selector.value;--%>
<%--                }--%>
<%--            </script>--%>


            <%--            <a href="${pageContext.request.contextPath}/in/download">--%>
            <%--                <button type="button" class="btn btn-danger btn-lg btn-block" >download</button>--%>
            <%--            </a>--%>

        </div>
    </div>
</div>


</body>
</html>
