<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 9/2/20
  Time: 3:20 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>target file upload</title>
</head>
<body>


<div class="container">
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
            <br><h5 align="center">Choose your target file ...</h5><br><br>
            <form method="post" action="${pageContext.request.contextPath}/in/targetFileUpload" enctype="multipart/form-data">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span style="width: 150px" class="input-group-text" id="inputGroup-sizing-default">Target file name</span>
                    </div>

                    <input type="text" name="targetFileName" class="form-control" required>
                </div>

                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <label style="width: 150px" class="input-group-text" for="typeSelector">target file
                            type</label>
                    </div>
                    <select class="custom-select" name="targetFileType" id="typeSelector">
                        <option selected>CSV</option>
                        <option>JSON</option>
                    </select>
                </div>

                <div class="input-group mb-3">
                    <div class="custom-file">
                        <input required type="file" class="custom-file-input" name="targetFile" formenctype="multipart/form-data">
                        <label  class="custom-file-label" aria-describedby="inputGroupFileAddon02" >Choose file</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary btn-lg btn-block">Next</button>
                <br>
                <a href=${pageContext.request.contextPath}/in/sourceFile>
                    <button type="button" class="btn btn-primary btn-lg btn-block">back</button>
                </a>
            </form>
        </div>
    </div>
</div>

<script>
    $('.custom-file-input').on('change', function () {
        let fileName = $(this).val().split('\\').pop();
        $(this).siblings('.custom-file-label').addClass('selected').html(fileName);
    });
</script>

</body>
</html>
