import com.progressoft.jip9.reconciliation.model.users.dao.UserDao;
import com.progressoft.jip9.reconciliation.model.users.dao.UserDaoImpl;
import com.progressoft.jip9.reconciliation.model.users.entity.User;
import com.progressoft.jip9.reconciliation.model.util.DataSourceFactory;
import com.progressoft.jip9.reconciliation.model.util.PasswordHashing;
import com.progressoft.jip9.reconciliation.database.DatabaseCrudImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.sql.Connection;
import java.sql.SQLException;

public class UserDatabaseTest {
    private Connection connection;

    @BeforeEach
    public void creatDataBase() {
        connection = DataSourceFactory.getConnection();
        DatabaseCrudImpl databaseCrud = new DatabaseCrudImpl(connection);
        databaseCrud.createTables();
    }

    @AfterEach
    public void closeConnection() throws SQLException {
        connection.close();
    }

    @Test
    public void givenValidConnection_whenAddAndGetUserByName_thenResultShouldBeAsExpected() {

        User user = new User("ahmed" , "password");

        UserDao userDao = new UserDaoImpl();
        userDao.addUser(user);

        User actualUser = userDao.getByName("ahmed");
        User expectedUser = new User("ahmed" , PasswordHashing.getHashed("password"));

        Assertions.assertEquals(expectedUser.getUserName() , actualUser.getUserName() , "not as expected");
        Assertions.assertEquals(expectedUser.getPassword() , actualUser.getPassword() , "not as expected");

    }

    @Test
    public  void givenValidConnection_whenInsertUser_thenPasswordShouldBeEncrypted(){
        User user = new User("ahmed" , "password");
        UserDao userDao = new UserDaoImpl();
        userDao.addUser(user);
        User actualUser = userDao.getByName("ahmed");

        Assertions.assertNotEquals("password" , actualUser.getPassword());

    }
}
