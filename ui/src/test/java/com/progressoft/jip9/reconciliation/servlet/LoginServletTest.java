package com.progressoft.jip9.reconciliation.servlet;

import com.progressoft.jip9.reconciliation.model.users.dao.UserDaoImpl;
import com.progressoft.jip9.reconciliation.model.users.entity.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;


public class LoginServletTest {


    @Mock
    HttpSession session;

    @Mock
    RequestDispatcher dispatcher;

    @Mock
    HttpServletRequest request;

    @Mock
    HttpServletResponse response ;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void givenInvalidUserNameAndPassword_whenDoLogin_thenFailToLogIn() throws ServletException, IOException {
        when(request.getSession()).thenReturn(session);
        when(request.getParameter("userName")).thenReturn("ahmed@ps.com");
        when(request.getParameter("password")).thenReturn("invalid");
        when(request.getRequestDispatcher("/WEB-INF/welcome.jsp")).thenReturn(dispatcher);

        LoginServlet loginServlet = new LoginServlet(new UserDaoImpl());

        loginServlet.doPost(request, response);


        verify(request, atLeast(1)).setAttribute("message", "invalid user name / password");
        verify(dispatcher).forward(request, response);
        verify(request, never()).getSession();


    }


    @Test
    public void givenValidUserNameAndPassword_whenDoLogin_thenLogIn() throws ServletException, IOException {

        when(request.getSession()).thenReturn(session);
        when(request.getParameter("userName")).thenReturn("ahmed@ps.com");
        when(request.getParameter("password")).thenReturn("ahmed");

        LoginServlet loginServlet = new LoginServlet(new UserDaoImpl());
        User validUser = new User ("ahmed@ps.com" , "ahmed");

        loginServlet.doPost(request, response);

        verify(session, atLeast(1)).setAttribute("user" , validUser);
        verify(response).sendRedirect(request.getContextPath()+"/in/sourceFile");


    }


}
