package com.progressoft.jip9.reconciliation.servlet;

import com.progressoft.jip9.reconciliation.model.files.FileMetaData;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;

import java.io.IOException;
import java.io.InputStream;

import static org.mockito.Mockito.*;

public class UploadSourceFileTest {


    @Mock
    HttpSession session;

    @Mock
    RequestDispatcher dispatcher;

    @Mock
    HttpServletRequest request;

    @Mock
    HttpServletResponse response ;

    @Mock
    InputStream input;

    @Mock
    Part part ;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void givenValidInputs_whenUploadSourceFile_thenGoToTargetFilePage() throws ServletException, IOException {
        when(request.getRequestDispatcher("/WEB-INF/views/targetFile.jsp")).thenReturn(dispatcher);
        when(request.getPart("sourceFile")).thenReturn(part);
        when(request.getSession()).thenReturn(session);
        when(session.getId()).thenReturn("sessionId");
        when( request.getSession().getId().concat("_source")).thenReturn("_source");
        when(part.getInputStream()).thenReturn(input);

        when(request.getParameter("sourceFileName")).thenReturn("srcFile");
        when(request.getParameter("sourceFileType")).thenReturn("srcType");
        when( part.getSubmittedFileName()).thenReturn("myFile");

        FileMetaData sourceFileMetaData = new FileMetaData("srcFile" ," srcType" ,"myFile");

        SourceFileUploadServlet uploadServlet = new SourceFileUploadServlet();
        uploadServlet.doPost(request , response);

//        verify(request.getSession() ).setAttribute("sourceFileMetaData" , sourceFileMetaData);
        verify(request.getRequestDispatcher("/WEB-INF/views/targetFile.jsp")).forward(request,response);
    }
}
