package com.progressoft.jip9.reconciliation.servlet;

import com.progressoft.jip9.reconciliation.model.files.FileMetaData;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class UploadTargetFileTest {

    @Mock
    HttpSession session;

    @Mock
    RequestDispatcher dispatcher;

    @Mock
    HttpServletRequest request;

    @Mock
    HttpServletResponse response ;

    @Mock
    InputStream input;

    @Mock
    Part part ;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void givenValidInputs_whenUploadTargetFile_thenGoToSummaryPage() throws ServletException, IOException {
        when(request.getRequestDispatcher("/WEB-INF/views/filesSummary.jsp")).thenReturn(dispatcher);

        when(request.getPart("targetFile")).thenReturn(part);
        when(request.getSession()).thenReturn(session);
        when(session.getId()).thenReturn("sessionId");
        when( request.getSession().getId().concat("_target")).thenReturn("_target");
        when(part.getInputStream()).thenReturn(input);
        when(request.getParameter("targetFileName")).thenReturn("trgtFile");
        when(request.getParameter("targetFileType")).thenReturn("trgtType");
        when( part.getSubmittedFileName()).thenReturn("myFile");

        FileMetaData targetFileMetaData = new FileMetaData("trgtFile" ," trgtType" ,"myFile");

        TargetFileUploadServlet uploadServlet = new TargetFileUploadServlet();
        uploadServlet.doPost(request , response);

//        verify(request.getSession() ).setAttribute("targetFileMetaData" , targetFileMetaData);
        verify(request.getRequestDispatcher("/WEB-INF/views/filesSummary.jsp")).forward(request,response);
    }
}
