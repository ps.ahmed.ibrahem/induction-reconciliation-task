<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 9/9/20
  Time: 4:26 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Activities Log</title>
</head>
<body>

            <table class="table table-dark" >
                <thead>
                <tr>
                    <th scope="col">Session time</th>
                    <th scope="col">Reconciliation time</th>
                    <th scope="col">Source file name</th>
                    <th scope="col">Target file name</th>
                    <th scope="col">Matching</th>
                    <th scope="col">Mismatching</th>
                    <th scope="col">Missing</th>
                </tr>
                </thead>
                <tbody>
                <c:forEach items="${sessionScope.activities}" var="a">
                    <tr>
                        <td>${a.sessionTime}</td>
                        <td>${a.reconTime}</td>
                        <td>${a.srcFileName}</td>
                        <td>${a.trgtFileName}</td>
                        <td>${a.matched}</td>
                        <td>${a.mismatched}</td>
                        <td>${a.missing}</td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
<br>



</body>
</html>
