<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 9/2/20
  Time: 3:20 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>source file upload</title>
</head>
<body>


<div class="container"  >
    <div class="row">
        <div class="col-sm-9 col-md-7 col-lg-7 mx-auto">
            <br><h5 align="center">you are ready !</h5><br>
            <div class="float-lg-left">
            <div class="card bg-light mb-3" style="max-width: 18rem;">
                <div class="card-header">Source file</div>
                <div style="width: 650px" class="card-body">
                    <p class="card-text">file name : ${sessionScope.sourceFileMetaData.fileName}</p>
                    <p class="card-text">file type : ${sessionScope.sourceFileMetaData.fileType} </p>
                </div>
            </div>
            </div>
            <div class="float-lg-right">
                <div class="card text-white bg-dark mb-3" style="max-width: 18rem;">
                    <div class="card-header">Target file</div>
                    <div style="width: 650px" class="card-body">
                        <p class="card-text">file name : ${sessionScope.targetFileMetaData.fileName} </p>
                        <p class="card-text">file type : ${sessionScope.targetFileMetaData.fileType} </p>
                    </div>
                </div>
            </div>

            <a href="${pageContext.request.contextPath}/in/compare">
            <button type="button" class="btn btn-success btn-lg btn-block" >compare</button><br>
            </a>
            <a href="${pageContext.request.contextPath}/in/targetFile">
            <button type="submit" class="btn btn-primary btn-lg btn-block" >back</button><br>
            </a>

        </div>
    </div>
</div>

<script>
    $('.custom-file-input').on('change', function () {
        let fileName = $(this).val().split('\\').pop();
        $(this).siblings('.custom-file-label').addClass('selected').html(fileName);
    });
</script>

</body>
</html>
